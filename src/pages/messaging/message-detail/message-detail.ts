import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { CreateMessagePage } from "../create-message/create-message";
import { RestProvider } from "../../../providers/rest/rest";
import { constants } from "../../../providers/constants";
import { SharedServiceProvider } from "../../../providers/shared-service/shared-service";

/**
 * Generated class for the MessagingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-message-detail",
  templateUrl: "message-detail.html"
})
export class MessageDetail {
  messageType = "mailBox";
  message_Details: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider
  ) {
    this.message_Details = navParams.data;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MessagingPage");
  }
  ionViewWillEnter() {
    if (this.message_Details.statu_read == "0") {
      let input = new FormData();
      input.append("id_mail_prive", this.message_Details.id_mail_prive);
      this.restService
        .postDataObservable(constants.UNREADMESSAGE, input)
        .subscribe(
          res => {
            if (res.status) {
            } else {
              this.sharedService.presentToast(res.message);
            }
          },
          err => {
            this.sharedService.presentToast(err.error.message);
            this.sharedService.dismissLoader();
            console.log(err);
          }
        );
    }
  }

  fnGotoCreateMessage(input) {
    let obj = { ...this.message_Details };
    obj.text =
      "------Envoyé le :" +
      (this.message_Details.created_m
        ? this.message_Details.created_m
        : this.message_Details.sended_m) +
      "------" +
      "\n" +
      this.message_Details.text;
    obj.objet = input + ": " + obj.objet;
    obj.type = input;

    this.navCtrl.push(CreateMessagePage, obj);
  }
  setcolor(category) {
    if (category == "frais") {
      return "frais";
    } else if (category == "etudes") {
      return "etudes";
    } else {
      return "autres";
    }
  }
}
