import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { SharedServiceProvider } from '../../providers/shared-service/shared-service';
import { RestProvider } from '../../providers/rest/rest';
import { storedConstants, constants } from '../../providers/constants';
import { NewsDetailPage } from '../news-detail/news-detail';

/**
 * Generated class for the FavorisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-favoris',
  templateUrl: 'favoris.html',
})
export class FavorisPage {
  id_titeur: any;
  cards: any;
  randomArray: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider) {
  }
  ionViewWillEnter() {
    var element = document.getElementById("myDIV");
    element.classList.remove("has-refresher");
    if (this.sharedService.checkForLoginStatus()) {
      this.sharedService.presentLoading();
      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      ).data.id_titeur;

      let input = new FormData();
      input.append("id_titeur", this.id_titeur);

      this.restService.postDataObservable(constants.NEWS_FAVOURITE, input).subscribe(
        res => {
          if (res.status) {
            if (res.news_data && res.news_data.length > 0) {

              this.cards = res.news_data.filter(id => {
                id.created = id.created.replace(/\s/g, "T");
                return id;
              });
            } else {
              this.sharedService.presentToast("No data available");
            }

            this.sharedService.dismissLoader();
          } else {
            this.sharedService.presentToast(res.message);
            this.sharedService.dismissLoader();
          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          console.log(err);
        }
      );
    }
  }
  gotoNewsDetail(main) {
    if (this.cards.length >= 3) {
      this.randomGenerator(3, this.cards);
    }
    else {
      this.randomGenerator(this.cards.length, this.cards);
    }
    if (this.randomArray) {
      this.navCtrl.push(NewsDetailPage, {
        main: main,
        // other: [this.cards[i + 1], this.cards[i + 2], this.cards[i + 3]]
        other: this.randomArray,
        cards: this.cards
      });
    }
  }
  randomGenerator(i, array) {
    let card = [];
    for (let j = 0; j < i; j++) {
      card.push(array[Math.floor(Math.random() * array.length)]);
    }
    console.log(card);
    return this.checkFORUnique(card, i);
  }
  checkFORUnique(array, len) {
    var result = array
      .map(item => item.id_news)
      .filter((value, index, self) => self.indexOf(value) === index);
    console.log(array);
    if (result.length !== len) {
      this.randomGenerator(len, this.cards);
      // return false;
    } else {
      this.randomArray = array;
    }
  }

  doRefresh(refresher) {
    console.log(refresher);
    console.log("Begin async operation", refresher);

    this.ionViewWillEnter();
    setTimeout(() => {
      console.log("Async operation has ended");
      refresher.complete();
    }, 2000);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FavorisPage');
  }
  favChange(newsId) {
    console.log(newsId)
    if (this.sharedService.checkForLoginStatus() && newsId) {


      this.sharedService.presentLoading();
      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      ).data.id_titeur;
      let input = new FormData();
      input.append("titeur_id", this.id_titeur);
      input.append("news_id", newsId.id_news);
      this.restService.postDataObservable(constants.UPDATEFAVOURITE, input).subscribe(
        res => {
          if (res.status) {
            this.ionViewWillEnter();
            this.sharedService.presentToast(res.message);

            this.sharedService.dismissLoader();

          } else {
            this.sharedService.presentToast(res.message);

            this.sharedService.presentToast(res.message);
            this.sharedService.dismissLoader();

          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();

          console.log(err);
        }
      );
    }
  }
}
