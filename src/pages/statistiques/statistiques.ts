import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { DataProvider } from "../../providers/data/data";
import { storedConstants, constants } from "../../providers/constants";

/**
 * Generated class for the StatistiquesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-statistiques",
  templateUrl: "statistiques.html"
})
export class StatistiquesPage {
  segment_one = "Systèmes";
  segment_two = "Messagerie";

  systemData = [];
  ParrainageData = [];
  NombreData = [];
  MessageData = [];
  TeachPlanData = [];
  id_titeur: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider
  ) {
    // this.systemData = [
    //   {
    //     id: 1,
    //     Mouvement: "Connexion ",
    //     date: "505",
    //     icon: "exit"
    //   },
    //   {
    //     id: 2,
    //     Mouvement: "Mise à jour profile  ",
    //     date: "2   / la dernière fois le: 23-10-2018 à 17:32",
    //     icon: "flash"
    //   }
    // ];
    // this.ParrainageData = [
    //   {
    //     id: 1,
    //     desctiption: "Personnes",
    //     count: "8",
    //     icon: "person"
    //   },
    //   {
    //     id: 2,
    //     desctiption: "liens ouverts",
    //     count: "4",
    //     icon: "logo-steam"
    //   }
    // ];
    // this.NombreData = [
    //   {
    //     id: 1,
    //     desctiption: "total",
    //     count: "5",
    //     icon: ""
    //   },
    //   {
    //     id: 2,
    //     desctiption: "garçon",
    //     count: "3",
    //     icon: ""
    //   },
    //   {
    //     id: 3,
    //     desctiption: "file",
    //     count: "2",
    //     icon: ""
    //   }
    // ];
    // this.TeachPlanData = [
    //   {
    //     titel: "Total Tâches",
    //     count: 16
    //   },
    //   {
    //     titel: "Tâche(s) effectuée(s)",
    //     count: 6
    //   }
    // ];
  }

  ionViewWillLoad() {
    console.log("ionViewDidLoad StatistiquesPage");
    if (this.sharedService.checkForLoginStatus()) {
      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      ).data.id_titeur;
      let input = new FormData();
      input.append("id_titeur", this.id_titeur);
      this.restService
        .postDataObservable(constants.STATISTIQUES, input)
        .subscribe(
          data => {
            if (data.status) {
              console.log(data.statitiques_data);
              this.MessageData = data.statitiques_data.messagerie1;
              this.TeachPlanData = data.statitiques_data.messagerie2;

              this.systemData = data.statitiques_data.tab1;
              this.ParrainageData = data.statitiques_data.tab2;
              this.NombreData = data.statitiques_data.tab3;
            } else {
              this.sharedService.presentToast(data.message);
              this.sharedService.dismissLoader();
            }
          },
          err => {
            this.sharedService.presentToast(err.message);
            this.sharedService.dismissLoader();
            console.log(err);
          }
        );
    }
  }
}
