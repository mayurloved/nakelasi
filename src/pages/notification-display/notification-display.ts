import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NotificationDisplayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-notification-display',
  templateUrl: 'notification-display.html',
})
export class NotificationDisplayPage {
  notificatioData: any;
  title: any;
  message: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.notificatioData = this.navParams.data;
    this.title = this.notificatioData.notification.title;
    this.message = this.notificatioData.notification.message;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationDisplayPage');
  }

}
