import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { HttpModule } from "@angular/http";
import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { LoginPage } from "../pages/login/login";
import { RegisterPage } from "../pages/register/register";
import { ChartsModule } from "ng2-charts";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { MessagingPage } from "../pages/messaging/messaging";
import { NewsDetailPage } from "../pages/news-detail/news-detail";
import { RestProvider } from "../providers/rest/rest";
import { DataProvider } from "../providers/data/data";
import { SharedServiceProvider } from "../providers/shared-service/shared-service";
import { ConnectivityProvider } from "../providers/connectivity/connectivity";
import { Network } from "@ionic-native/network";
import { DetailPage } from "../pages/children/detail/detail";
import { ListPage } from "../pages/children/list/list";
import { CreateMessagePage } from "../pages/messaging/create-message/create-message";
import { MessageDetail } from "../pages/messaging/message-detail/message-detail";
import { SchoolNewsReleasesPage } from "../pages/messaging/school-news-releases/school-news-releases";
import { ReleaseDetailPage } from "../pages/messaging/release-detail/release-detail";
import { AboutSchoolPage } from "../pages/children/about-school/about-school";
import { PipesModule } from "../pipes/pipes.module";
// import { GooglePlus } from "@ionic-native/google-plus";
// import { Facebook } from "@ionic-native/facebook";
import { ForgotpasswordPage } from "../pages/forgotpassword/forgotpassword";
import { JournalClasse } from "../pages/children/detail/journal_classe/journal_classe";
import { UserprofilePage } from "../pages/userprofile/userprofile";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Partenaires } from "../pages/partenaires/partenaires";
import { AboutEcole } from "../pages/aboutEcole/aboutEcole";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { ParrainagesPage } from "../pages/parrainages/parrainages";
import { Temoignages } from "../pages/temoignages/temoignages";
import { ResetpasswordPage } from "../pages/resetpassword/resetpassword";
import { Supports } from "../pages/supports/supports";
import { StatistiquesPage } from "../pages/statistiques/statistiques";
// import { CalendarComponent } from "ap-angular-fullcalendar";
import { CalendarComponent } from "ap-angular2-fullcalendar/src/calendar/calendar";

import { CalendarPage } from "../pages/calendar/calendar";
import { AddeventPage } from "../pages/calendar/addevent/addevent";
import * as $ from "jquery";
import { ControleDetailPage } from "../pages/children/detail/controle-detail/controle-detail";
import { PasswordStrengthMeterModule } from "angular-password-strength-meter";
import { AproposPage } from "../pages/apropos/apropos";
import { FavorisPage } from "../pages/favoris/favoris";
import { NotificationDisplayPage } from "../pages/notification-display/notification-display";
import { AppVersion } from '@ionic-native/app-version';
import { SocialSharing } from '@ionic-native/social-sharing';
import { IntroPage } from "../pages/intro/intro";
import { LegalTextsPage } from "../pages/legalTexts/legalTexts";
import { HistoryTokenPage } from '../pages/history-token/history-token';
import {EnPage} from '../pages/en/en';
import { Clipboard } from '@ionic-native/clipboard';

//For language translation
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    IntroPage,
    ListPage,
    LoginPage,
    RegisterPage,
    DetailPage,
    SchoolNewsReleasesPage,
    MessagingPage,
    CreateMessagePage,
    NewsDetailPage,
    MessageDetail,
    ReleaseDetailPage,
    AboutSchoolPage,
    ForgotpasswordPage,
    JournalClasse,
    UserprofilePage,
    Partenaires,
    AboutEcole,
    ParrainagesPage,
    Temoignages,
    ResetpasswordPage,
    Supports,
    StatistiquesPage,
    CalendarComponent,
    CalendarPage,
    AddeventPage,
    ControleDetailPage,
    AproposPage,
    FavorisPage,
    NotificationDisplayPage,
    LegalTextsPage,
    HistoryTokenPage,
    EnPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ChartsModule,
    IonicModule.forRoot(MyApp, {
      // mode: "md"
    }),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    PasswordStrengthMeterModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    IntroPage,
    ListPage,
    LoginPage,
    RegisterPage,
    DetailPage,
    SchoolNewsReleasesPage,
    MessagingPage,
    CreateMessagePage,
    NewsDetailPage,
    MessageDetail,
    ReleaseDetailPage,
    AboutSchoolPage,
    ForgotpasswordPage,
    JournalClasse,
    UserprofilePage,
    Partenaires,
    AboutEcole,
    ParrainagesPage,
    Temoignages,
    ResetpasswordPage,
    Supports,
    StatistiquesPage,
    CalendarPage,
    AddeventPage,
    ControleDetailPage,
    AproposPage,
    FavorisPage,
    NotificationDisplayPage,
    LegalTextsPage,
    HistoryTokenPage,
    EnPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AppVersion,
    InAppBrowser,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    RestProvider,
    SharedServiceProvider,
    DataProvider,
    ConnectivityProvider,
    Network,
    // GooglePlus,
    // Facebook,
    SocialSharing,
    Clipboard
  ]
})
export class AppModule { }
