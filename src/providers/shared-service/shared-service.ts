import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {
  LoadingController,
  ToastController,
  NavController,
  Events,
  Platform
} from "ionic-angular";
import { DataProvider } from "../data/data";
import { storedConstants } from "../constants";
import { LoginPage } from "../../pages/login/login";
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";
import { NotificationDisplayPage } from "../../pages/notification-display/notification-display";

declare let cordova: any;
/*
 created by :Paras Shah
 created date: 25 Oct, 2018
 purpose : central service for shared UI components like loaders, toasters, pop-up models. 
*/
@Injectable()
export class SharedServiceProvider {

  PUSHWOOSH_APP_ID: string = '00E87-73724'; // Your pushwoosh app id.
  GOOGLE_PROJECT_NUMBER: string = '777189002964'; // Project number from firebase.

  public loader;
  resgistrationObj: any;
  // user_Details = new Subject<any>();
  constructor(
    public http: HttpClient,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    // private geolocation: Geolocation,
    public data: DataProvider,
    public platform: Platform,
    public events: Events // private navCtrl: NavController
  ) {
    // console.log("Hello SharedServiceProvider Provider");
    // this.CheckForPushPermission();
    this.platform.ready().then(() => {
      if (this.platform.is('ios')) {
        console.log("PushwooshService init: Running on push compatible platform " + this.platform.userAgent() + ')');
        this.initIOS();
      } else {
        if (this.platform.is('android')) {
          console.log("PushwooshService init: Running on push compatible platform " + this.platform.userAgent() + ')');
          this.initAndroid();
        }
        else {
          console.log("PushwooshService init: No compatible platform available.  Skipping init.");
          return;
        }
      }
    });

  }
  initIOS() {
    let pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");
    let e = this.events;
    // Set push notification callback before we initialize the plugin.
    document.addEventListener('push-notification', function (event: any) {
      // Get the notification payload.
      let notification = event.notification;
      let title = event.notification.title;
      let message = event.notification.message;
      let userData = event.notification.userdata;

      if (message) {



        e.publish("NotificationPage", event);


      }
      // Display alert to the user for example.
      alert(notification.aps.alert);

      // Clear the app badge.
      pushNotification.setApplicationIconBadgeNumber(0);
    });

    // Initialize the plugin.
    pushNotification.onDeviceReady({ pw_appid: this.PUSHWOOSH_APP_ID });

    // Register for pushes.
    pushNotification.registerDevice(
      function (status) {
        console.log("IOS status from register", JSON.stringify(status));
        let deviceToken = status['pushToken'];
        console.warn('registerDevice: ' + deviceToken);
        // this.resgistrationObj = deviceToken;
        console.log("IOS token", deviceToken)
        e.publish("Notification:Token", deviceToken);
      },
      function (status) {
        console.warn('failed to register : ' + JSON.stringify(status));
        alert(JSON.stringify(['failed to register ', status]));
      }
    );

    // Reset badges on app start.
    pushNotification.setApplicationIconBadgeNumber(0);
  }

  initAndroid() {
    let pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");
    let e = this.events;
    // Set push notifications handler.
    document.addEventListener('push-notification', function (event: any) {
      // console.log('user data 1: ' + JSON.stringify(event));

      let title = event.notification.title;
      let message = event.notification.message;
      let userData = event.notification.userdata;

      if (typeof (userData) != "undefined") {
        console.log('user data 1.1: ' + JSON.stringify(userData));
      }
      if (message) {
        // // alert(message);
        // this.presentToast(message);
        // this.navCtrl.push(NotificationDisplayPage, event);
        //  // e.publish("NotificationPage", event);


      }
      // let toast = this.toastCtrl.create({
      //   message: message,
      //   duration: 4000,
      //   position: "top"
      // });

      // toast.present();
    });
    document.addEventListener('push-receive', function (event: any) {
      let title = event.notification.title;
      let message = event.notification.message;
      let userData = event.notification.userdata;
      console.log('user data 2: ' + JSON.stringify(event));

      if (userData) {
        console.log('user data 2.1 : ' + JSON.stringify(userData));
      }
      // this.presentToast(message);
      if (message) {


        // this.navCtrl.push(NotificationDisplayPage, event);
        e.publish("NotificationPage", event);

        // let toast = this.toastCtrl.create({
        //   message: message,
        //   duration: 4000,
        //   position: "top"
        // });

        // toast.present();
      }
    });
    // Initialize Pushwoosh with projectid: GOOGLE_PROJECT_NUMBER, pw_appid : PUSHWOOSH_APP_ID. This will trigger all
    // pending push notifications on start.
    pushNotification.onDeviceReady({
      projectid: this.GOOGLE_PROJECT_NUMBER,
      pw_appid: this.PUSHWOOSH_APP_ID
      // serviceName: "MPNS_SERVICE_NAME"
    });

    // Register for pushes.
    pushNotification.registerDevice(
      function (status) {
        var pushToken = status['pushToken'];
        console.log('push token: ' + pushToken);
        e.publish("Notification:Token", pushToken);
        // alert('push token: ' + JSON.stringify(pushToken));
      },
      function (status) {
        console.warn(JSON.stringify(['failed to register ', status]));
      }
    );

    // Sets a string tag “username” with value “adrian”.
    // pushNotification.setTags({ username: "adrian", },
    //   function (status) {
    //     console.warn('setTags success');
    //   },
    //   function (status) {
    //     console.warn('setTags failed');
    //   }
    // );
  }

  checkForLoginStatus() {
    // let nav = this.navCtrl;
    if (this.data.getFromLocalStorage(storedConstants.USER_DETAILS)) {
      return true;
    } else {
      this.events.publish("session:expired");
      // nav.setRoot(LoginPage)
    }
  }
  // getLoggedInUserDetails(): Observable<any> {
  //   if (this.checkForLoginStatus()) {
  //     this.user_Details.next(
  //       this.data.getFromLocalStorage(storedConstants.USER_DETAILS)
  //     );
  //     return this.user_Details.asObservable();
  //   }
  // }

  ///Loaders
  presentLoading(msg = "chargement…") {
    if (!this.loader) {
      this.loader = this.loadingCtrl.create({
        spinner: "hide",
        content:
          "<img style='background: #2a3343'  src='./assets/imgs/icon.png' /> <div style='color: #2a3343' >" +
          msg +
          "</div>"
      });
      this.loader.present();
    }
  }

  dismissLoader() {
    if (this.loader) {
      this.loader.dismiss();
      this.loader = null;
    }
  }

  ///Toasters
  presentToast(msg, duration = 3000) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: "top"
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }

  //Get Current Location
  getCurrentLocation(options?) {
    // return this.geolocation.getCurrentPosition(options);
  }
  CheckForPushPermission() {
    // to check if we have permission
    // this.push.hasPermission().then((res: any) => {
    //   if (res.isEnabled) {
    //     console.log("We have permission to send push notifications");
    //   } else {
    //     console.log("We do not have permission to send push notifications");
    //   }
    // });
    // const options: PushOptions = {
    //   android: {},
    //   ios: {
    //     alert: "true",
    //     badge: true,
    //     sound: "false"
    //   },
    //   windows: {},
    //   browser: {
    //     pushServiceURL: "http://push.api.phonegap.com/v1/push"
    //   }
    // };

    // const pushObject: PushObject = this.push.init(options);

    // pushObject.on("notification").subscribe((notification: any) => {
    //   console.log("Received a notification", notification);
    //   if (notification) {
    //     this.presentToast(notification.title);
    //   }
    // });

    // pushObject.on("registration").subscribe((registration: any) => {
    //   console.log("Device registered", registration);
    //   this.resgistrationObj = registration;
    // });
  }
  getFcmToken() {
    return this.resgistrationObj ? this.resgistrationObj : "";
  }
}
