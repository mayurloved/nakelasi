import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { constants, storedConstants } from "../../providers/constants";
import { RegisterPage } from "../register/register";
import { ResetpasswordPage } from "../resetpassword/resetpassword";
import { LoginPage } from "../login/login";
import { DataProvider } from "../../providers/data/data";

/**
 * Generated class for the ForgotpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-forgotpassword",
  templateUrl: "forgotpassword.html"
})
export class ForgotpasswordPage {
  emailError: boolean;
  passwordError: boolean;
  email: string;
  school = "";
  isFieldError: boolean;
  verificationProccess: boolean;
  schools = [];

  code = "";
  schoolError: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sharedService: SharedServiceProvider,
    private dataService: DataProvider,
    private rest: RestProvider
  ) { }
  navToReg() {
    this.navCtrl.push(RegisterPage);
  }
  navToLogin() {
    this.navCtrl.push(LoginPage);
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad ForgotpasswordPage");
    this.verificationProccess = false;
    if (!this.sharedService.checkForLoginStatus()) {
      this.rest.getDataObservable(constants.SCHOOLLIST).subscribe(schools => {
        if (schools.status) {
          this.schools = schools.data;
          this.sharedService.dismissLoader();
        }
      });

    }
  }
  validation(from) {
    switch (from) {
      case "email":
        this.emailError = false;
        break;
      case "password":
        this.passwordError = false;
        break;
      case "school":
        this.schoolError = false;
        break;
    }
  }

  doForgotPass() {
    if (
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        this.email
      )
    ) {
      this.isFieldError = false;
    } else {
      this.emailError = true;
      this.isFieldError = true;
    }

    if (this.school == "" || this.school == undefined || this.school == null) {
      this.isFieldError = true;
      this.schoolError = true;
    }
    if (this.emailError) {
      return false;
    }
    if (this.email == undefined || this.email == "" || this.schoolError) {
      // let toast = this.toastCtrl.create({
      //   message: "Please fill all details",
      //   duration: 2000,
      //   position: "bottom",
      //   showCloseButton: true,
      //   closeButtonText: "ok"
      // });
      // toast.present();
    } else {
      if (this.school) {
        this.schools.filter(id => {
          if (id.id_school == this.school) {
            console.log(id.urlschool);
            this.rest.BaseUrl = id.urlschool + "/api/";
            this.dataService.localeStorage(storedConstants.DYNAMIC_BASE_URL, id.urlschool + "/api/");
          }
        })
      }
      this.sharedService.presentLoading();
      let input = new FormData();
      input.append("forgot_email", this.email);

      this.rest.postDataObservable(constants.FORGOTPASSWORD, input).subscribe(
        data => {
          if (data.status) {
            this.sharedService.dismissLoader();
            this.verificationProccess = true;

            // this.is_login = true;
          } else {
            this.sharedService.dismissLoader();
            this.sharedService.presentToast(data.error);
          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          this.navCtrl.pop();
          console.log(err);
        }
      );
    }
  }
  verifyPass() {
    if (this.verificationProccess) {
      this.sharedService.presentLoading();
      let input = new FormData();
      input.append("email", this.email);
      input.append("recup_code", this.code);

      this.rest.postDataObservable(constants.FORGOTPASSWORD, input).subscribe(
        data => {
          if (data.status) {
            this.sharedService.dismissLoader();

            this.navCtrl.push(ResetpasswordPage, { email: this.email });

            // this.is_login = true;
          } else {
            this.sharedService.dismissLoader();
            this.sharedService.presentToast(data.error);
          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          console.log(err);
        }
      );
    }
  }
}
