import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { constants, storedConstants } from "../../providers/constants";
import { DataProvider } from "../../providers/data/data";
/**
 * Generated class for the ParrainagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-parrainages",
  templateUrl: "parrainages.html"
})
export class ParrainagesPage {
  emailError = false;
  prenomDestinationError = false;
  nomDestinationError = false;
  descriptionError = true;
  email: any;
  preDesti: any;
  nomDesti: any;
  description: any =
    "Bonjour ! Je vous recommande l’application NaKELASI qui est intéressant pour nous parents. Cette application nous donne la possibilité de suivre en temps réel l’évolution scolaire de nos enfants. Si l’école de votre enfant ne l’a pas encore officialisé, veuillez leur demander de le mettre en place. Pour plus de contact veuillez vous renseigner au www.nakelasi.com";

  responseData = [
    // {
    //   id: 1,
    //   title: " Sandra wanet",
    //   status: "Acceptée",
    //   date: " le 28.12.2018 à 08:30 "
    // },
    // {
    //   id: 2,
    //   title: " Maman Ka kambay",
    //   status: "En attente...",
    //   date: "Supprimer"
    // },
    // {
    //   id: 3,
    //   title: " judith musau",
    //   status: "En attente...",
    //   date: "Supprimer"
    // },
    // {
    //   id: 4,
    //   title: "perrot mamba",
    //   status: "Acceptée",
    //   date: " le 28.12.2018 à 08:30 "
    // },
    // {
    //   id: 5,
    //   title: "nathalie muland",
    //   status: "Acceptée",
    //   date: " le 28.12.2018 à 08:30 "
    // }
  ];
  id_titeur: any;
  id_email: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public rest: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider
  ) { }

  ionViewWillLoad() {
    this.getList();
    console.log("ionViewDidLoad ParrainagesPage");
  }
  getList() {
    this.id_email = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    ).data.email;
    if (this.sharedService.checkForLoginStatus()) {
      this.sharedService.presentLoading();
      // id_titeur,tuteur,message,prenomch,nomch
      let input = new FormData();
      input.append("titeur_email", this.id_email);

      this.rest.postDataObservable(constants.PARRAINAGES, input).subscribe(
        data => {
          if (data.status) {
            this.responseData = data.invitation_list.filter(id => {
              if (id.accepted_date) {

                id.accepted_date = id.accepted_date.replace(/\s/g, "T");
              }
              return id;
            });
            this.sharedService.dismissLoader();
          } else {
            // this.mainProvider.hideLoading();
            this.sharedService.dismissLoader();

            this.sharedService.presentToast(data.error[0]);
          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          console.log(err);
        }
      );
    }
  }
  validation(from) {
    switch (from) {
      case "email":
        if (
          !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            this.email
          )
        ) {
          this.emailError = true;
        } else {
          this.emailError = false;
        }

        break;
      case "prenomDestination":
        this.prenomDestinationError = false;
        break;
      case "nomDestination":
        this.nomDestinationError = false;
        break;

      case "description":
        this.descriptionError = false;
        break;
    }
  }

  fnSendParrentEmail() {
    if (this.email == "" || this.email == undefined || this.email == null) {
      this.emailError = true;
    } else if (
      !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        this.email
      )
    ) {
      this.emailError = true;
    } else if (
      this.preDesti == "" ||
      this.preDesti == undefined ||
      this.preDesti == null
    ) {
      this.prenomDestinationError = true;
    } else if (
      this.nomDesti == "" ||
      this.nomDesti == undefined ||
      this.nomDesti == null
    ) {
      this.nomDestinationError = true;
    } else if (
      this.description == "" ||
      this.description == undefined ||
      this.description == null
    ) {
      this.descriptionError = true;
    } else {
      console.log("success");
      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      ).data.id_titeur;
      this.sharedService.presentLoading();
      // id_titeur,tuteur,message,prenomch,nomch
      let input = new FormData();
      input.append("id_titeur", this.id_titeur);

      input.append("tuteur", this.email);
      input.append("prenomch", this.preDesti);
      // input.append("mobile", this.phno);
      input.append("nomch", this.nomDesti);
      input.append("message", this.description);

      this.rest.postDataObservable(constants.PARRAINAGES, input).subscribe(
        data => {
          if (data.status) {
            this.sharedService.presentToast(data.message);
            this.sharedService.dismissLoader();
          } else {
            // this.mainProvider.hideLoading();
            this.sharedService.dismissLoader();

            this.sharedService.presentToast(data.error[0]);
          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          console.log(err);
        }
      );
    }
  }
  doRefresh(refresher) {
    // this.selectedDate = "";

    console.log(refresher);
    console.log("Begin async operation", refresher);
    this.getList();

    setTimeout(() => {
      console.log("Async operation has ended");
      refresher.complete();
    }, 2000);
  }
}
