import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

/**
 * Generated class for the ReleaseDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-release-detail",
  templateUrl: "release-detail.html"
})
export class ReleaseDetailPage {
  schoolNews: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.schoolNews = this.navParams.data;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ReleaseDetailPage");
    console.log(this.navParams.data);
  }
}
