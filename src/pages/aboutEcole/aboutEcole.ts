import { Component, ViewChild } from "@angular/core";
import { NavController, NavParams, Content } from "ionic-angular";
import { DataProvider } from "../../providers/data/data";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { RestProvider } from "../../providers/rest/rest";
import { constants, storedConstants } from "../../providers/constants";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";

/**
 * Generated class for the NewsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 * https://medium.com/@AAlakkad/angular-2-display-html-without-sanitizing-filtering-17499024b079
 */

@Component({
  selector: "page-aboutEcole",
  templateUrl: "aboutEcole.html"
})
export class AboutEcole {
  id_titeur: any;
  aboutDetail: any;
  @ViewChild(Content) content: Content;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider,
    private iab: InAppBrowser
  ) {}

  ionViewWillEnter() {
    this.getAboutEcole();
  }

  getAboutEcole() {
    this.sharedService.presentLoading();
    this.id_titeur = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    ).data.id_titeur;
    let input = new FormData();
    input.append("id_titeur", this.id_titeur);

    this.restService.postDataObservable(constants.ABOUTECOLE, input).subscribe(
      res => {
        if (res.status) {
          this.aboutDetail = res.about_ecole;
          this.sharedService.dismissLoader();
        } else {
          this.sharedService.dismissLoader();
        }
      },
      err => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        console.log(err);
      }
    );
  }
  openLink(link) {
    // window.open(link);
    link = "http://" + link;
    const browser = this.iab.create(link, "_system");
  }
}
