import { Component } from "@angular/core";
import { NavController, NavParams, Events } from "ionic-angular";
import { DataProvider } from "../../../../providers/data/data";
import { SharedServiceProvider } from "../../../../providers/shared-service/shared-service";
import { RestProvider } from "../../../../providers/rest/rest";
import { constants } from "../../../../providers/constants";
import * as moment from "moment";

/**
 * Generated class for the MessagingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-journal_classe",
  templateUrl: "journal_classe.html",
})
export class JournalClasse {
  messageType = "mailBox";
  category_current: any;
  items = [];
  id_titeur: any;
  result: any;
  childId: any;
  data = [];
  childDetail: any;

  dayTask: any;

  selectValue: any;

  filterVar: any;
  resultData: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider,
    public events: Events
  ) {
    this.childId = navParams.data.id_eleve;
    this.childDetail = navParams.data;
    // console.log(this.childId);
  }

  ionViewDidLoad() {
    // console.log(this.navParams.data);
  }
  ionViewWillEnter() {
    if (this.childId) {
      this.getJournelData();
    }
  }
  getJournelData() {
    if (this.sharedService.checkForLoginStatus()) {
      this.sharedService.presentLoading();
      let input = new FormData();
      input.append("id_eleve", this.childId);
      this.restService
        .postDataObservable(constants.JOURNAL_CLASSES, input)
        .subscribe(
          (res) => {
            if (res.status) {
              console.log(res);
              this.data = res.journal_classes_list;
              this.resultData = this.data;
              this.sharedService.dismissLoader();
            } else {
              this.sharedService.presentToast(res.message);
              this.sharedService.dismissLoader();
            }
          },
          (err) => {
            this.sharedService.presentToast(err.error.message);
            this.sharedService.dismissLoader();
            console.log(err);
          }
        );
    }
  }

  FilterData() {
    switch (this.selectValue) {
      case "auj":
        this.dayTask = "Aujourd'hui";
        // code block
        break;
      case "hier":
        this.dayTask = "Hier";
        // code block
        break;
      case "avhier":
        this.dayTask = "Avant Hier";
        // code block
        break;
      case "mois_actuel":
        this.dayTask = "Ce mois-ci";
        // code block
        break;
      case "tous":
        this.dayTask = "Tous...";
        // code block
        break;
      default:
      // code block
    }

    //today
    if (this.selectValue == "auj") {
      this.filterVar = moment(new Date(), "YYYY-MM-DD").format("YYYY-MM-DD");
      // console.log(this.filterVar);
    }
    //yesterday
    if (this.selectValue == "hier") {
      this.filterVar = moment(new Date())
        .subtract(1, "day")
        .format("YYYY-MM-DD");
      // console.log(this.filterVar);
    }
    //before yesterday
    if (this.selectValue == "avhier") {
      this.filterVar = moment(new Date())
        .subtract(2, "day")
        .format("YYYY-MM-DD");
      // console.log(this.filterVar);
    }
    //this month
    if (this.selectValue == "mois_actuel") {
      this.filterVar = moment(new Date(), "YYYY-MM-DD").format("YYYY-MM");
      // console.log(this.filterVar);
    }
    //all

    if (this.selectValue != "tous") {
      this.data = this.resultData.filter((item) => {
        let date: any = item.created;
        if (this.selectValue == "mois_actuel") {
          date = moment(date).format("YYYY-MM");
        } else {
          date = moment(date).format("YYYY-MM-DD");
        }
        if (date == this.filterVar) {
          return item;
        }
      });
      return this.data;
    } else {
      this.data = this.resultData;
      return this.data;
    }
  }
}
