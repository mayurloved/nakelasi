import { Component, ViewChild, OnInit } from "@angular/core";
import {
  NavController,
  NavParams,
  MenuController,
  Slides,
} from "ionic-angular";
import { RestProvider } from "../../../providers/rest/rest";
import { constants } from "../../../providers/constants";
import { SharedServiceProvider } from "../../../providers/shared-service/shared-service";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { JournalClasse } from "./journal_classe/journal_classe";
import { ControleDetailPage } from "./controle-detail/controle-detail";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
// import Chart from "chart.js";
@Component({
  selector: "page-detail",
  templateUrl: "detail.html",
})
export class DetailPage implements OnInit {
  @ViewChild("slider") slider: Slides;
  abonnementForm: FormGroup;
  abonnementFormSubmitted: any = false;

  jatonAbonnementForm: FormGroup;
  jatonAbonnementFormSubmitted: any = false;
  remainingDaysPer: any;
  remainingDays: any;
  expiryDate: any;
  responseText: any;
  selectedItem: any;
  icons: string[];
  childDetail: any;
  course = [];
  controls_detail = [];
  numberOfPassed = 0;
  finalPercentage: any = "";
  classmate = [];
  professor: any;
  // graphFlag: boolean;
  selector: any;
  attandence_selected: any;
  percent_sec;
  chart: any;
  selectedDate = "";
  selectedDates = [];
  showSelectedDateError: boolean;
  finalAttendants: any;
  segment1 = "mes_notes";
  segment2 = "recharger_jeton";

  showAbonnement = false;
  responseAPIStatus = true

  constructor(
    public formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    public rest: RestProvider,
    public sharedService: SharedServiceProvider
  ) {
    this.childDetail = navParams.data;
    this.getRemainingDays(this.childDetail.id_eleve);
    if (this.childDetail.authen == "1") {
      this.getchildDetails(this.childDetail.id_eleve);
    }
    this.clearData();
    // If we navigated to this page, we will have an item available as a nav param

    // this.chart = new Chart({

    //   options: {
    //     // ...
    //   }
    // });
  }

  public lineChartData: Array<any> = [{ data: [], label: "Réussites" }];
  public lineChartData1: Array<any> = [{ data: [], label: "Réussites" }];
  public lineChartLabels: Array<any> = [
    "January",
    "February",
    "March",
    "April",
    "May",
  ];
  public lineChartOptions: any = {
    plugins: {
      plugins: [ChartDataLabels],
      // Change options for ALL labels of THIS CHART
      datalabels: {
        color: "#ffffff",
        font: { size: "0" },
        formatter: function(value, context) {
          return Math.round(value) + "%";
        },
      },
    },
    responsive: true,
    scales: {
      xAxes: [
        {
          gridLines: {
            drawOnChartArea: false,
            display: false,
          },
          ticks: {
            display: false,
          },
        },
      ],
      yAxes: [
        {
          gridLines: {
            drawOnChartArea: false,
            display: false,
          },
          ticks: {
            display: false,
          },
        },
      ],
    },
  };
  public lineChartColors: Array<any> = [
    {
      // grey
      backgroundColor: "rgb(244, 251, 243)",
      borderColor: "rgb(40, 168, 11)",
      pointBackgroundColor: "rgb(40, 168, 11)",
      borderWidth: 5,
      pointBorderWidth: 5,

      // pointBorderColor: '#fff',
      // pointHoverBackgroundColor: '#fff',
      // pointHoverBorderColor: 'rgba(148,159,177,0.8)',
    },
  ];
  public lineChartLegend: boolean = true;
  public lineChartType: string = "line";

  public doughnutChartLabels: string[] = ["Reussite", "Echec"];
  public doughnutChartData: number[] = [];
  public doughnutChartType: string = "pie";
  public doughnutColors: Array<any> = [
    {
      // grey
      // backgroundColor: ["#2483bc", "#ff7f0e"],
      backgroundColor: ["green", "red"],
      // borderColor: "rgb(40, 168, 11)",
      pointBackgroundColor: "rgb(40, 168, 11)",
      // borderWidth: 5,
      // pointBorderWidth: 5
    },
  ];
  doughnutChartOptions = {
    plugins: {
      plugins: [ChartDataLabels],
      // Change options for ALL labels of THIS CHART
      datalabels: {
        color: "#ffffff",
        font: { size: "14", weight: "600" },
        formatter: function(value, context) {
          return value.toFixed(1) + "%";
        },
      },
      // legend: {
      //   display: true,
      //   labels: {
      //     fontColor: "rgb(255, 99, 132)"
      //   }
      // }
    },
  };

  ngOnInit(): void {
    this.abonnementForm = this.formBuilder.group({
      telephone: ["", Validators.required],
      qte: ["1", [Validators.required, Validators.max(5), Validators.min(1)]],
      devise: ["", Validators.required]
    });
    this.jatonAbonnementForm = this.formBuilder.group({
      codeJaton: ["", Validators.required],
    });
  }
  serchDateChanged() {
    this.getchildDetails(this.childDetail.id_eleve);
  }
  dateChanged() {
    const selectedDate = new Date(this.selectedDate);
    if (this.controls_detail.length > 0) {
      this.selectedDates = this.controls_detail.filter((it) => {
        const tableDate = new Date(it.created);
        if (tableDate.getTime() == selectedDate.getTime()) {
          return it;
        }
      });
      if (this.selectedDates.length == 0) {
        this.showSelectedDateError = true;
      } else {
        this.showSelectedDateError = false;
      }
    }

    console.log(this.selectedDates);
    // }
  }
  clearData() {
    this.selector = null;
    this.course = [];
    this.controls_detail = [];
    this.numberOfPassed = 0;
    this.finalPercentage = "";
    this.classmate = [];
    this.professor = null;
    this.selectedDates = [];
    this.showSelectedDateError = false;
  }

  getRemainingDays(id) {
    this.sharedService.presentLoading();
    let input = new FormData();
    input.append("id_eleve", id);
    input.append("type", "remaining_days");
    this.rest.postDataObservable(constants.CHILD_DETAIL, input).subscribe(
      (res) => {
        console.log(res);
        if (res.status) {
          this.expiryDate = res.expiry_date;
          this.remainingDays = res.remaining_days;
          this.remainingDaysPer = (100 * res.remaining_days) / 30;
          this.sharedService.dismissLoader();
        } else {
          this.sharedService.presentToast(res.message);
          this.sharedService.dismissLoader();
        }
      },
      (err) => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        console.log(err);
      }
    );
  }
  getchildDetails(id) {
    this.clearData();
    this.sharedService.presentLoading();
    let input = new FormData();
    input.append("id_eleve", id);
    this.rest.postDataObservable(constants.CHILD_DETAIL, input).subscribe(
      (res) => {
        console.log(res);
        if (res.status) {
          this.course = res.course;
          this.classmate = res.classmate;
          this.professor = res.professor;
          this.controls_detail = res.controls_detail.map((id) => {
            if (id.point_maximum / 2 > id.point_obtenu) {
              id.pass = false;
            } else {
              id.pass = true;
              this.numberOfPassed++;
            }
            return id;
          });

          if (this.controls_detail.length >= 0) {
            this.finalPercentage =
              this.controls_detail.length > 0
                ? (
                    (100 / this.controls_detail.length) *
                    this.numberOfPassed
                  ).toFixed(2)
                : "0";
            //second percentage
            var value_sum = 0;
            let max_sum = 0;
            this.controls_detail.filter((o) => {
              value_sum = value_sum + +o.point_obtenu;
              max_sum = max_sum + +o.point_maximum;
            });

            this.percent_sec =
              max_sum > 0 ? ((100 * value_sum) / max_sum).toFixed(1) : 0;
            //piechart data
            // this.graphFlag = false;

            let doughnutChartData = this.doughnutChartData;
            doughnutChartData[0] = +this.finalPercentage;
            doughnutChartData[1] =
              +this.finalPercentage != 0 ? 100 - doughnutChartData[0] : 0;

            this.doughnutChartData = doughnutChartData;
            //line chart data

            this.lineChartData[0].data = this.controls_detail.map((id) => {
              return +id.point_obtenu * 2;
            });
            this.lineChartData1[0].data = this.controls_detail.map((id) => {
              return +id.point_obtenu;
            });
            this.lineChartLabels = this.controls_detail.map((id) => {
              return id.created;
            });
            console.log(this.lineChartData);

            // this.graphFlag = true;

            if (this.selectedDate) {
              this.dateChanged();
            }
          }
          this.sharedService.dismissLoader();
        } else {
          this.sharedService.presentToast(res.message);
          this.sharedService.dismissLoader();
        }
      },
      (err) => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        console.log(err);
      }
    );
  }
  // events
  public chartClicked(e: any): void {
    console.log(e);
  }
  // navToChildDetail(i) {
  //   this.navCtrl.push(DetailPage, i);
  // }
  public chartHovered(e: any): void {
    console.log(e);
  }
  linegraphChanged() {
    this.selectedDates = [];
    this.selectedDate = "";
    this.reDecorateLinechart(this.selector);
    // this.clearData();
  }
  attandanceChanged() {
    this.sharedService.presentLoading();
    let input = new FormData();
    input.append("eleveID", this.childDetail.id_eleve);
    input.append("presence", this.attandence_selected);
    this.rest.postDataObservable(constants.ATTENDANT, input).subscribe(
      (res) => {
        console.log(res);
        if (res.status) {
          this.finalAttendants = res.attendants;
          console.log(res);

          this.sharedService.dismissLoader();
        }
      },
      (err) => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        console.log(err);
      }
    );
  }
  reDecorateLinechart(selector) {
    this.sharedService.presentLoading();
    let input = new FormData();
    input.append("eleveID", this.childDetail.id_eleve);
    input.append("chart", selector);
    this.rest.postDataObservable(constants.CHART, input).subscribe(
      (res) => {
        console.log(res);
        if (res.status) {
          this.numberOfPassed = 0;
          this.controls_detail = res.chart_info.map((id) => {
            if (id.point_maximum / 2 > id.point_obtenu) {
              id.pass = false;
            } else {
              id.pass = true;
              this.numberOfPassed++;
            }
            return id;
          });
          console.log(this.controls_detail);
          if (this.controls_detail.length >= 0) {
            this.finalPercentage =
              this.controls_detail.length > 0
                ? (
                    (100 / this.controls_detail.length) *
                    this.numberOfPassed
                  ).toFixed(2)
                : "0";

            //second percentage
            var value_sum = 0;
            let max_sum = 0;
            this.controls_detail.filter((o) => {
              value_sum = value_sum + +o.point_obtenu;
              max_sum = max_sum + +o.point_maximum;
            });

            this.percent_sec =
              max_sum > 0 ? ((100 * value_sum) / max_sum).toFixed(1) : 0;

            //piechart data
            // this.graphFlag = false;
            // let doughnutChartData = this.doughnutChartData;
            this.doughnutChartData[0] = +this.finalPercentage;
            this.doughnutChartData[1] =
              +this.finalPercentage != 0 ? 100 - this.doughnutChartData[0] : 0;

            // this.doughnutChartData = doughnutChartData;
            //line chart data
            this.lineChartData[0].data = this.controls_detail.map((id) => {
              return +id.point_obtenu * 2;
            });
            this.lineChartData1[0].data = this.controls_detail.map((id) => {
              return +id.point_obtenu;
            });
            this.lineChartLabels = this.controls_detail.map((id) => {
              return id.created;
            });
            console.log(this.lineChartData);

            // this.graphFlag = true;
          }
          this.sharedService.dismissLoader();
        }
      },
      (err) => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        console.log(err);
      }
    );
  }

  ionViewDidLoad() {
    this.menu.swipeEnable(false);
  }
  doRefresh(refresher) {
    this.selectedDate = "";

    console.log(refresher);
    console.log("Begin async operation", refresher);
    this.getchildDetails(this.childDetail.id_eleve);

    setTimeout(() => {
      console.log("Async operation has ended");
      refresher.complete();
    }, 2000);
  }
  navToJournalClasse() {
    this.navCtrl.push(JournalClasse, this.childDetail);
  }
  navTocontrolDetail() {
    this.navCtrl.push(ControleDetailPage, this.childDetail);
  }

  segment1Changed(ev: any) {
    console.log("Segment changed", ev);
  }

  segment2Changed(ev: any) {
    console.log("Segment changed", ev);
  }

  changeSegment() {
    this.showAbonnement = true;
    this.segment1 = "mes_notes";
  }

  abonnementClose() {
    this.showAbonnement = false;
    this.segment2 = "recharger_jeton";
  }

  abonnementClick() {
    this.abonnementFormSubmitted = true;
    if (this.abonnementForm.valid) {
      this.sharedService.presentLoading();
      this.abonnementFormSubmitted = false;
      let formValues = this.abonnementForm.value;
      var data = new FormData();
      data.append("id_titeur", this.childDetail.id_titeur);
      data.append("id_eleve", this.childDetail.id_eleve);
      data.append("qte", formValues.qte);
      data.append("telephone", formValues.telephone);
      data.append("devise", formValues.devise);
      this.rest
        .postDataObservable(constants.PAYEMENT, data)
        .subscribe(async(detail) => {
          if (detail.status) {
            this.abonnementForm.reset();
            this.sharedService.dismissLoader()
            await this.callGetResponseAPI(detail.refference)
          }
        });
    } else {
      return false;
    }
  }

  callGetResponseAPI(refference) {
    this.responseAPIStatus = true;
    this.responseText = "";
    for (let i = 0; i < 12; i++) {
      this.sharedService.presentLoading(); 
        setTimeout(() =>
          this.callResponseAPI(refference, i), i
        * 4000);
      }
  }

  callResponseAPI(refference, count) {
  if (this.responseAPIStatus === true) {
    var data = new FormData();
    data.append("refference", refference);
    this.rest
      .postDataObservable(constants.GET_RESPONSE, data)
      .subscribe((data) => {
        // if (data.payment_result_code === '0') {
        //   this.responseAPIStatus = false;
        //   this.sharedService.dismissLoader()
        //   this.responseText = data.message;
        //   // setTimeout(() => {
        //   //   this.responseText = "";
        //   // }, 5000);
        // }  else if (data.payment_result_code != null) {
        //   this.responseAPIStatus = false;
        //   this.sharedService.dismissLoader()
        //   this.responseText = data.message;
        //   // setTimeout(() => {
        //   //   this.responseText = "";
        //   // }, 5000);
        // }
        if (data.payment_result_code != null) {
          this.responseAPIStatus = false;
          this.sharedService.dismissLoader()
          this.responseText = data.message;
        }
      });
    }
  }

  jatonAbonnementClick() {
    // console.log("smksm");
    this.jatonAbonnementFormSubmitted = true;
    console.log(this.jatonAbonnementForm.valid);
    if (this.jatonAbonnementForm.valid) {
      this.jatonAbonnementFormSubmitted = false;
      let formValues = this.jatonAbonnementForm.value;
      this.sharedService.presentLoading();
      var data = new FormData();
      data.append("id_eleve", this.childDetail.id_eleve);
      data.append("code_jeton", formValues.codeJaton);
      this.rest
        .postDataObservable(constants.HISTORY_JETON, data)
        .subscribe((detail) => {
          console.log(data);
          if (detail.status) {
            this.sharedService.dismissLoader();
            this.jatonAbonnementForm.reset();
            this.getRemainingDays(this.childDetail.id_eleve);
            this.sharedService.presentToast(detail.message);
            this.showAbonnement = false;
          } else {
            this.sharedService.dismissLoader();
            this.sharedService.presentToast(detail.message);
          }
        });
    } else {
      return false;
    }
  }
}
