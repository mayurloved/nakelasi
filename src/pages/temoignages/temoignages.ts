import { Component, ViewChild } from "@angular/core";
import { NavController, NavParams, Content } from "ionic-angular";
import { DataProvider } from "../../providers/data/data";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { RestProvider } from "../../providers/rest/rest";
import { constants, storedConstants } from "../../providers/constants";
import { getLocaleFirstDayOfWeek } from "@angular/common";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";

/**
 * Generated class for the NewsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 * https://medium.com/@AAlakkad/angular-2-display-html-without-sanitizing-filtering-17499024b079
 */

@Component({
  selector: "page-temoignages",
  templateUrl: "temoignages.html"
})
export class Temoignages {
  @ViewChild(Content) content: Content;
  id_titeur: any;
  selector: any;
  message: any;
  temolistdata: any;
  added_Temolistdata: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider
  ) {}

  ionViewWillEnter() {
    this.getListData();
  }

  getListData() {
    this.id_titeur = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    ).data.id_titeur;
    let input = new FormData();
    input.append("id_titeur", this.id_titeur);

    this.restService
      .postDataObservable(constants.LIST_TEMOIGNAGES, input)
      .subscribe(
        res => {
          if (res.status) {
            this.temolistdata = res.temoignages_data;
            this.added_Temolistdata = res.temoignages_data[0];
          } else {
            this.sharedService.dismissLoader();
            this.sharedService.presentToast(res.error);
          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          console.log(err);
        }
      );
  }

  addTemognages() {
    if (
      this.message == "" ||
      this.message == undefined ||
      this.message == null
    ) {
      this.sharedService.presentToast(
        "Erreur Impossible de soumettre un formulaire vide"
      );
      return false;
    }
    this.id_titeur = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    ).data.id_titeur;
    let input = new FormData();
    input.append("id_titeur_add", this.id_titeur);
    input.append("temoignage", this.message);

    this.restService
      .postDataObservable(constants.LIST_TEMOIGNAGES, input)
      .subscribe(
        res => {
          if (res.status == true) {
            this.added_Temolistdata = res.temoignages_data[0];
            console.log(JSON.stringify(this.added_Temolistdata));
            this.sharedService.presentToast(res.message);
            this.message = "";
          } else {
            this.sharedService.dismissLoader();
            this.sharedService.presentToast(res.error);
          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          console.log(err);
        }
      );
  }

  updateTemognages(temoId) {
    if (
      this.selector == "" ||
      this.selector == undefined ||
      this.selector == null
    ) {
      this.sharedService.presentToast(
        "Erreur Impossible de soumettre un formulaire vide"
      );
      return false;
    }
    this.id_titeur = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    ).data.id_titeur;
    let input = new FormData();
    input.append("id_temoignage", temoId);
    input.append("auth", this.selector);

    this.restService
      .postDataObservable(constants.LIST_TEMOIGNAGES, input)
      .subscribe(
        res => {
          if (res.status == true) {
            this.getListData();
            this.sharedService.presentToast(res.message);
            this.selector = "";
            //  this.added_Temolistdata=res.temoignages_data[0];

            //  this.sharedService.presentToast(res.message);
          } else {
            this.sharedService.dismissLoader();
            this.sharedService.presentToast(res.error);
          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          console.log(err);
        }
      );
  }
}
