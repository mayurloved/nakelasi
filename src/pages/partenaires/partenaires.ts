import { Component, ViewChild } from "@angular/core";
import { NavController, NavParams, Content } from "ionic-angular";
import { DataProvider } from "../../providers/data/data";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { RestProvider } from "../../providers/rest/rest";
import { constants, storedConstants } from "../../providers/constants";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
/**
 * Generated class for the NewsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 * https://medium.com/@AAlakkad/angular-2-display-html-without-sanitizing-filtering-17499024b079
 */

@Component({
  selector: "page-partenaires",
  templateUrl: "partenaires.html"
})
export class Partenaires {
  @ViewChild(Content) content: Content;
  parteners = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider,
    private iab: InAppBrowser
  ) {}
  ionViewWillEnter() {
    this.getDetails();
  }
  getDetails() {
    if (this.sharedService.checkForLoginStatus()) {
      this.sharedService.presentLoading();
      let input = new FormData();
      // input.append("id_titeur", this.id_titeur);
      input.append("page_count", "1");
      this.restService
        .postDataObservable(constants.PARTENAIRES, input)
        .subscribe(
          data => {
            if (data.status) {
              console.log(data);
              this.parteners = data.parteners;
              this.sharedService.dismissLoader();
            }
          },
          err => {
            this.sharedService.presentToast(err.error.message);
            this.sharedService.dismissLoader();
            console.log(err);
          }
        );
    }
  }

  openLink(link) {
    // window.open(link);
    const browser = this.iab.create(link, "_system");
  }
}
