import { Component, ViewChild } from "@angular/core";
import { NavController, NavParams, ModalController } from "ionic-angular";
import { CalendarComponent } from "ap-angular2-fullcalendar/src/calendar/calendar";
import { RestProvider } from "../../providers/rest/rest";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { DataProvider } from "../../providers/data/data";
import { storedConstants, constants } from "../../providers/constants";
import { AddeventPage } from "./addevent/addevent";

/**
 * Generated class for the CalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-calendar",
  templateUrl: "calendar.html"
})
export class CalendarPage {
  calendarOptions: any;
  // @ViewChild(CalendarComponent) myCalendar: CalendarComponent;
  mainDate: any;
  mainEvent: any;
  id_titeur: any;
  displayFlag = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider,
    public modalController: ModalController
  ) {
    this.getEvents();
  }

  async presentModal(obj) {
    const modal = await this.modalController.create(AddeventPage, obj);
    modal.onDidDismiss(data => {
      console.log(data);
      this.getEvents();
    });
    return await modal.present();
  }
  ionViewWillLoad() {}
  getEvents() {
    this.displayFlag = false;

    if (this.sharedService.checkForLoginStatus()) {
      this.sharedService.presentLoading();
      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      ).data.id_titeur;
      let input = new FormData();
      input.append("id_titeur_calendar", this.id_titeur);
      this.restService.postDataObservable(constants.CALENDAR, input).subscribe(
        data => {
          if (data.status) {
            console.log(data);
            this.calendarOptions = {
              header: {
                left: "title",
                right: "month,agendaWeek,agendaDay,agendaFourDay,"
              },
              footer: {
                right: "today prev,next"
              },
              views: {
                agendaFourDay: {
                  type: "listYear",
                  buttonText: "All"
                }
              },
              // theme:'jquery-ui',
              height: "parent",
              fixedWeekCount: false,
              // defaultDate: Date(),
              editable: true,
              allDay: true,
              eventClick: event => {
                console.log(event);
                // alert("Event Name : " + new Date(event.start));
                this.mainEvent = event.title;
                event.start = new Date(event.start).toISOString();
                if (event.end) {
                  event.end = new Date(event.end).toISOString();
                }

                event.id_titeur = this.id_titeur;
                this.presentModal(event);
              },
              dayClick: (date, jsEvent, view, resourceObj) => {
                console.log("Date: " + date.format());
                this.mainDate = date.format();
                this.presentModal({
                  start: this.mainDate,
                  id_titeur: this.id_titeur
                });
              },
              eventLimit: true, // allow "more" link when too many events
              events: data.events.filter(id => {
                if (id.start) {
                  id.start = id.start.replace(/\s/g, "T");
                }
                if (id.end) {
                  id.end = id.end.replace(/\s/g, "T");
                }
                return id;
              })
            };
            this.displayFlag = true;
            this.sharedService.dismissLoader();
          } else {
            this.sharedService.presentToast(data.message);
            this.sharedService.dismissLoader();
          }
        },
        err => {
          this.sharedService.presentToast(err.message);
          this.sharedService.dismissLoader();
          console.log(err);
        }
      );
    }
  }
  ngAfterViewInit() {}
}
