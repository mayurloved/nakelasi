import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {
  constructor(public http: HttpClient) {
    // console.log("Hello DataProvider Provider");
  }
  public localeStorage(key, data) {
    localStorage[key] = data;
  }
  public getFromLocalStorage(key) {
    let res = key ? JSON.parse(localStorage.getItem(key)) : null;
    return res;
  }
}
