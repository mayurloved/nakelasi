export class constants {
  public static BASE_URL = "http://ss.nakelasi.com/api/";
  public static LOGIN = "login.php";
  public static REGISTERATION = "register.php";
  public static SCHOOLLIST = "school_list.php";

  public static HOME = "home.php";
  public static NEWS_FAVOURITE = "news_favourite.php";
  public static UPDATEFAVOURITE = "news_favourite.php";
  public static CHILDRENLIST = "child_list.php";
  public static CHILD_DETAIL = "child_detail.php";
  public static CHART = "chart.php";
  public static ATTENDANT = "attendant.php";
  public static INBOX = "inbox.php";
  public static COMPOSE_MAIL = "compose_mail.php";
  public static COMMUNIQUES = "communiques.php";
  public static ARTICLE = "article.php";
  public static UNREADMESSAGE = "inbox.php";
  public static JOURNAL_CLASSES = "journal_classes.php";
  public static PARTENAIRES = "partenaires.php";
  public static ABOUTECOLE = "about_ecole.php";
  public static PROFILE = "profile.php";
  public static PARRAINAGES = "parrainages.php";
  public static LIST_TEMOIGNAGES = "temoignages.php";
  public static LIST_SUPPORTS = "supports.php";
  public static STATISTIQUES = "statistiques.php";
  public static FORGOTPASSWORD = "forgot_password.php";
  public static CALENDAR = "calendar.php";

  public static SHARE_COUNT = "news_share.php";

  public static LEGAL_TEXTS = "legacy.php";
  public static PAYEMENT = "payement.php";
  public static HISTORY_JETON = "code_jeton_history.php";
  public static GET_RESPONSE = "getresponse.php";


}

export class storedConstants {
  public static USER_DETAILS = "nakelasi_user";
  public static USER_CRED_CHOICE = "nakelasi_user_credential_choice";
  public static DYNAMIC_BASE_URL = "DYNAMIC_BASE_URL";

  public static INTROVIDEO = "INTRO_VIDEO";
  public static INTRO = "INTRO";
}
