import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  Events,
  normalizeURL,
  Platform
} from "ionic-angular";
import { ActionSheet, ActionSheetOptions } from "@ionic-native/action-sheet";

import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { DataProvider } from "../../providers/data/data";
import { storedConstants, constants } from "../../providers/constants";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { RestProvider } from "../../providers/rest/rest";
import { ResetpasswordPage } from "../resetpassword/resetpassword";
import { Crop } from "@ionic-native/crop";
import {
  DomSanitizer,
  SafeResourceUrl,
  SafeUrl
} from "@angular/platform-browser";
import { File } from "@ionic-native/file";
/**
 * Generated class for the UserprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-userprofile",
  templateUrl: "userprofile.html",
  providers: [Camera, ActionSheet, Crop, File]
})
export class UserprofilePage {
  base64Image: any;
  croppedImage = null;
  profilePicture = null;
  userData: any;
  profileForm: FormGroup;
  disableFlag: boolean;
  imageError: boolean;
  profilePicture_uploadImage: any = "";
  isUploaded: boolean;
  submitted = false;
  profilePicture2 = null;
  profilePicture_uploadImage2: any = "";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public sharedService: SharedServiceProvider,
    public rest: RestProvider,
    public dataService: DataProvider,
    public fb: FormBuilder,
    private camera: Camera,
    private actionSheet: ActionSheet,
    public events: Events,
    private crop: Crop,
    // public base64: Base64,
    public domSanitizer: DomSanitizer,
    private file: File,
    public plateform: Platform
  ) {
    this.generateProfileForm();
  }
  get f() {
    return this.profileForm.controls;
  }
  ionViewDidLoad() {
    if (this.sharedService.checkForLoginStatus()) {
      this.sharedService.presentLoading();
      this.userData = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      );
      if (this.userData.profile_info_for_profile) {
        this.profilePicture = this.userData.profile_info_for_profile.photo
          ? this.userData.profile_info_for_profile.photo
          : "";
        this.profilePicture2 = this.userData.profile_info_for_profile.photo2
          ? this.userData.profile_info_for_profile.photo2
          : "";
        this.userData.profile_info_for_profile.date_born = this.formateDate(
          this.userData.profile_info_for_profile.date_born
        );
        //console.log(this.profileForm);
        //console.log(this.userData.profile_info_for_profile);
        var keys = Object.keys(this.userData.profile_info_for_profile);
        //console.log(keys);
        keys.forEach(element => {
          //console.log(this.userData.profile_info_for_profile[element]);
          if (this.userData.profile_info_for_profile[element] == null) {
            this.userData.profile_info_for_profile[element] = "";
          }
        });
        this.profileForm.patchValue(this.userData.profile_info_for_profile);
        //console.log(this.userData.profile_info_for_profile);

        //console.log(this.profileForm);
      } else {
        this.sharedService.presentToast("No Data found ");
      }
      // this.profileForm.disable();
      this.disableFlag = false;
      this.sharedService.dismissLoader();
    }
    //console.log("ionViewDidLoad UserprofilePage");
  }
  generateProfileForm() {
    this.profileForm = this.fb.group({
      email: new FormControl("", [Validators.email]),
      nomp: new FormControl(""),
      prenomp: new FormControl(""),
      postnomp: new FormControl(""),
      date_born: new FormControl(""),
      civilite: new FormControl(""),
      profession: new FormControl(""),
      adresse: new FormControl(""),
      telephone: new FormControl(""),
      parente: new FormControl(""),
      // form2
      email2: new FormControl("", [Validators.email]),
      nom_titeur2: new FormControl(""),
      prenom_titeur2: new FormControl(""),
      civilite2: new FormControl(""),
      profession2: new FormControl(""),
      adresse2: new FormControl(""),
      tel_titeur2: new FormControl(""),
      parente2: new FormControl("")
    });
  }
  formateDate(date) {
    const fromattedDate = new Date(date);
    //console.log(fromattedDate);
    const month = (fromattedDate.getMonth() + 1).toString();
    const seprateddate = fromattedDate.getDate().toString();
    return (
      fromattedDate.getFullYear() +
      "-" +
      // month +
      (month.length > 1 ? month : "0" + month) +
      "-" +
      // seprateddate
      (seprateddate.length > 1 ? seprateddate : "0" + seprateddate)
    );
  }
  enableProfileForm() {
    // this.profileForm.enable();
    this.disableFlag = false;

    // this.profileForm.updateValueAndValidity();
  }

  uploadProfilePic(numb) {
    let buttonLabels = ["Choose From Gallery", "Take Picture"];
    const options: ActionSheetOptions = {
      title: "Select Photo",
      buttonLabels: buttonLabels,
      addCancelButtonWithLabel: "Cancel",
      androidEnableCancelButton: true,
      // androidTheme: this.actionSheet.ANDROID_THEMES.THEME_HOLO_DARK,
      destructiveButtonLast: true
    };

    this.actionSheet.show(options).then((buttonIndex: number) => {
      if (buttonIndex == 1) {
        const cameraOptions: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
          allowEdit: false,
          encodingType: this.camera.EncodingType.JPEG,
          saveToPhotoAlbum: false,
          correctOrientation: true,
          targetWidth: 500,
          targetHeight: 500
        };

        this.croppedImage = null;
        this.camera.getPicture(cameraOptions).then(
          imageURI => {
            if (numb == 1) {
              this.cropImage(imageURI, 1);
            } else if (numb == 2) {
              this.cropImage(imageURI, 2);
            }
          },
          err => {}
        );
      } else if (buttonIndex == 2) {
        const cameraOptions: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.FILE_URI,
          sourceType: this.camera.PictureSourceType.CAMERA,
          allowEdit: false,
          encodingType: this.camera.EncodingType.JPEG,
          saveToPhotoAlbum: false,
          correctOrientation: true,
          targetWidth: 500,
          targetHeight: 500
        };

        this.camera.getPicture(cameraOptions).then(
          imageURI => {
            if (numb == 1) {
              this.cropImage(imageURI, 1);
            } else if (numb == 2) {
              this.cropImage(imageURI, 2);
            }
          },
          err => {}
        );
      }
    });
  }

  cropImage(img, numb) {
    this.crop.crop(img, { quality: 100 }).then(
      newImage => {
        // split file path to directory and file name
        let fileName = newImage.split("/").pop();
        if (this.plateform.is("android")) {
          fileName = fileName.substring(0, fileName.lastIndexOf("?"));
        }
        let path = newImage.substring(0, newImage.lastIndexOf("/") + 1);
        // let path = newImage;
        console.log("File name", fileName);
        console.log("File Path", path);

        this.file
          .readAsDataURL(path, fileName)
          .then(base64File => {
            console.log("here is encoded image ", base64File);
            if (numb == 1) {
              this.profilePicture_uploadImage = base64File.replace(
                /^data:image\/[a-z]+;base64,/,
                ""
              );
              this.profilePicture = null;
              this.profilePicture = base64File;
            } else if (numb == 2) {
              this.profilePicture_uploadImage2 = base64File.replace(
                /^data:image\/[a-z]+;base64,/,
                ""
              );
              this.profilePicture2 = base64File;
            }
          })
          .catch(() => {
            console.log("Error reading file");
            this.sharedService.presentToast("Error reading file ");
          });

        console.log("new image path is: " + newImage);
      },
      error => {
        console.error("Error cropping image", error);
        this.sharedService.presentToast("Error cropping image ");
      }
    );
  }

  navtoResetPassword() {
    this.navCtrl.push(ResetpasswordPage);
  }

  updateProfile() {
    this.submitted = true;
    if (this.profileForm.valid) {
      //console.log(this.profileForm.value);
      const data = this.profileForm.value;
      this.sharedService.presentLoading();

      let input = new FormData();
      input.append(
        "id_titeur_edit",
        this.userData.profile_info_for_profile.id_titeur
      );
      input.append("prenomp", data.prenomp);
      input.append("nomp", data.nomp);
      input.append("postnomp", data.postnomp);
      input.append("email", data.email);
      input.append("date_born", data.date_born);
      input.append("telephone", data.telephone);
      input.append("profession", data.profession);
      input.append("civilite", data.civilite);
      input.append("parente", data.parente);
      input.append("adresse", data.adresse);
      input.append("prenom_titeur2", data.prenom_titeur2);
      input.append("nom_titeur2", data.nom_titeur2);
      input.append("email2", data.email2);
      input.append("tel_titeur2", data.tel_titeur2);
      input.append("profession2", data.profession2);
      input.append("parente2", data.parente2);
      input.append("civilite2", data.civilite2);
      input.append("adresse2", data.adresse2);
      input.append("photo", this.profilePicture_uploadImage);
      input.append("photo2", this.profilePicture_uploadImage2);
      this.rest.postDataObservable(constants.PROFILE, input).subscribe(
        data => {
          if (data.status) {
            //console.log(data);
            this.sharedService.presentToast(data.message);
            this.userData.profile_info_for_profile = data.profile_data;
            this.dataService.localeStorage(
              storedConstants.USER_DETAILS,
              JSON.stringify(this.userData)
            );
            this.events.publish("profile_update", this.userData);
            this.profileForm.patchValue(data.profile_data);
            this.sharedService.dismissLoader();
          } else {
            this.sharedService.dismissLoader();
          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          //console.log(err);
        }
      );
    }
  }
  cancel() {
    this.navCtrl.pop();
  }
}
