import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  ViewController,
  AlertController,
  MenuController,
  ToastController
} from "ionic-angular";
import { ActionSheet, ActionSheetOptions } from "@ionic-native/action-sheet";
import { RestProvider } from "../../providers/rest/rest";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { HomePage } from "../home/home";
import { constants, storedConstants } from "../../providers/constants";
import { LoginPage } from "../login/login";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { DataProvider } from "../../providers/data/data";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-register",
  templateUrl: "register.html",
  providers: [Camera, ActionSheet]
})
export class RegisterPage {
  name: any;
  confirmpassword: any;
  email: any;
  password: any;
  phno: any;
  dob: any;
  converted_date: any;
  gender: any = "male";
  school;
  nameError = false;
  confirmpasswordError = false;
  emailError = false;
  passwordError = false;
  numberError = false;
  genderError = false;

  isFieldError = false;
  schools: any;

  profilePicture = "";
  isUploaded: boolean;
  profilePicture_uploadImage: any;
  // imageError: boolean;
  schoolError: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public menu: MenuController,
    public toastCtrl: ToastController,
    public rest: RestProvider,
    public sharedService: SharedServiceProvider,
    private camera: Camera,
    private actionSheet: ActionSheet,
    private dataService: DataProvider, // public translate: TranslateService

  ) { }
  ionViewDidLoad() {
    // this.mainProvider.getDeviceDetails();
    this.rest.getDataObservable(constants.SCHOOLLIST).subscribe(schools => {
      if (schools.status) {
        this.schools = schools.data;
        this.sharedService.dismissLoader();
      }
    });
  }

  validation(from) {
    switch (from) {
      case "name":
        this.nameError = false;
        break;
      case "confirmpassword":
        this.confirmpasswordError = false;
        break;
      case "email":
        this.emailError = false;
        break;
      case "password":
        this.passwordError = false;
        break;
      case "number":
        this.numberError = false;
        break;
      case "gender":
        this.genderError = false;
        break;
      case "school":
        this.schoolError = false;
        break;
    }
  }

  checkFields() {
    if (
      this.name == "" ||
      this.name == undefined ||
      (this.name == null && this.name.indexOf(" ") >= 0)
    ) {
      this.nameError = true;
      this.isFieldError = true;
    } else {
      this.isFieldError = false;
    }

    if (
      this.confirmpassword == "" ||
      this.confirmpassword == undefined ||
      this.confirmpassword == null ||
      this.confirmpassword != this.password
    ) {
      this.confirmpasswordError = true;
      this.isFieldError = true;
    } else {
      this.isFieldError = false;
    }

    if (this.email == "" || this.email == undefined || this.email == null) {
      this.emailError = true;
      this.isFieldError = true;
    } else {
      this.isFieldError = false;
    }

    if (
      this.password == "" ||
      this.password == undefined ||
      this.password == null
    ) {
      this.passwordError = true;
      this.isFieldError = true;
    } else {
      this.isFieldError = false;
    }
  }

  doRegistraton() {
    if (!this.name || /^\s*$/.test(this.name)) {
      this.nameError = true;
      this.isFieldError = true;
    } else {
      this.isFieldError = false;
    }

    if (!this.confirmpassword || this.confirmpassword != this.password) {
      this.confirmpasswordError = true;
      this.isFieldError = true;
    } else {
      this.isFieldError = false;
    }

    if (this.school == "" || this.school == undefined || this.school == null) {
      this.isFieldError = true;
      this.schoolError = true;
    } else {
      this.isFieldError = false;
    }

    if (
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        this.email
      )
    ) {
      this.isFieldError = false;
    } else {
      this.emailError = true;
      this.isFieldError = true;
    }

    if (/[0-9]/.test(this.phno)) {
      this.isFieldError = false;
    } else {
      this.numberError = true;
      this.isFieldError = true;
    }

    if (
      this.password == "" ||
      this.password == undefined ||
      this.password == null
    ) {
      this.passwordError = true;
      this.isFieldError = true;
    } else {
      this.isFieldError = false;
    }
    // if (this.profilePicture_uploadImage) {
    //   this.imageError = false;
    // } else {
    //   this.imageError = true;

    //   this.isFieldError = true;
    // }

    if (
      this.nameError ||
      this.confirmpasswordError ||
      this.emailError ||
      this.schoolError ||
      this.passwordError
      // this.imageError
    ) {
      return false;
    }
    if (this.dob != null) {
      this.converted_date = this.dob
        .split("/")
        .reverse()
        .join(".");
      console.log(this.converted_date);
    }
    // if (this.name == undefined || this.name == '' || this.confirmpassword == undefined || this.confirmpassword == '' || this.email == undefined || this.email == '' || this.password == undefined || this.password == '') {
    // 	let toast = this.toastCtrl.create({
    // 		message: 'Please fill all details',
    // 		duration: 2000,
    // 		position: 'bottom',
    // 		showCloseButton: true,
    // 		closeButtonText: 'ok'
    // 	  });
    // 	  toast.present();
    // }
    // else {
    if (this.school) {
      this.schools.filter(id => {
        if (id.id_school == this.school) {
          console.log(id.urlschool);
          this.rest.BaseUrl = id.urlschool + "/api/";
          this.dataService.localeStorage(storedConstants.DYNAMIC_BASE_URL, id.urlschool + "/api/");
        }
      })
    }
    this.sharedService.presentLoading();
    // let headers = new Headers({
    //   "Content-Type": "application/x-www-form-urlencoded"
    // });
    // let options = new RequestOptions({
    // 	headers: headers
    // });
    // let params = 'email=' + this.email + '&password=' + this.password + '&name=' + this.name + '&confirmpassword=' + this.confirmpassword + '&dob=' + this.converted_date + '&phno=' + this.phno + '&gender=' + this.gender + '&device_type=' + this.mainProvider.device_type + '&token=' + Device.uuid + '&lat=' + this.mainProvider.lat + '&lng=' + this.mainProvider.lng;
    let input = new FormData();
    input.append("name", this.name);
    input.append("email", this.email);
    input.append("school_id", this.school);

    // input.append("mobile", this.phno);
    input.append("password", this.password);
    input.append("profile_image", this.profilePicture_uploadImage);
    // input.append("gender", this.gender);
    // input.append("dob", this.dob);
    this.rest.postDataObservable(constants.REGISTERATION, input).subscribe(
      data => {
        if (data.status) {
          this.sharedService.dismissLoader();
          // localStorage["User"] = JSON.stringify(data);
          //   localStorage["shoppen_userdata"] = JSON.stringify(data.data);
          // this.mainProvider.actionAlert('Registration Successfull', MainCategory)
          let prompt = this.alertCtrl.create({
            title: "Nakelasi",
            message:
              "vérifier votre mail pour activer votre compte",
            cssClass: "alert-success",
            buttons: [
              {
                text: "OK",
                handler: data => {
                  this.navCtrl.setRoot(LoginPage);
                  // this.menu.open();
                  // setTimeout(() => {
                  // 	this.menu.close();
                  // }, 800);
                  // this.mainProvider.hideLoading();
                }
              }
            ]
          });
          prompt.present();
        } else {
          // this.mainProvider.hideLoading();
          this.sharedService.dismissLoader();

          this.sharedService.presentToast(data.error[0]);
        }
      },
      err => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        console.log(err);
      }
    );
    // }
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  datePicker() {
    let options = {
      date: new Date(),
      mode: "date"
    };
    // DatePicker.show(options).then(
    // 	date => {
    // 		let month = date.getMonth() + 1;
    // 		this.dob = date.getDate() + '/' + month + "/" + date.getFullYear();
    // 	},
    // 	error => {
    // 		console.log(error);
    // 	}
    // )
  }

  uploadProfilePic() {
    let buttonLabels = ["Choose From Gallery", "Take Picture"];
    const options: ActionSheetOptions = {
      title: "Select Photo",
      buttonLabels: buttonLabels,
      addCancelButtonWithLabel: "Cancel",
      androidEnableCancelButton: true,
      // androidTheme: this.actionSheet.ANDROID_THEMES.THEME_HOLO_DARK,
      destructiveButtonLast: true
    };

    this.actionSheet.show(options).then((buttonIndex: number) => {
      if (buttonIndex == 1) {
        const cameraOptions: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
          allowEdit: false,
          encodingType: this.camera.EncodingType.JPEG,
          saveToPhotoAlbum: false,
          correctOrientation: true,
          targetWidth: 250,
          targetHeight: 250
        };

        this.camera.getPicture(cameraOptions).then(
          imageURI => {
            // this.imageError = false;
            this.profilePicture = "data:image/jpeg;base64," + imageURI;
            this.profilePicture_uploadImage = imageURI;
            this.isUploaded = true;
          },
          err => { }
        );
      } else if (buttonIndex == 2) {
        const cameraOptions: CameraOptions = {
          quality: 100,
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType: this.camera.PictureSourceType.CAMERA,
          allowEdit: false,
          encodingType: this.camera.EncodingType.JPEG,
          saveToPhotoAlbum: false,
          correctOrientation: true,
          targetWidth: 250,
          targetHeight: 250
        };

        this.camera.getPicture(cameraOptions).then(
          imageURI => {
            // this.imageError = false;
            this.profilePicture = "data:image/jpeg;base64," + imageURI;
            this.profilePicture_uploadImage = imageURI;
            this.isUploaded = true;
          },
          err => { }
        );
      }
    });
  }

  // convertImg(base64) {
  //   // Base64 url of image
  //   // let base64="/9j/4AAQSkZJRgABAQE...";
  //   // Naming the image
  //   const date = new Date().valueOf();
  //   let text = "";
  //   const possibleText =
  //     "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  //   for (let i = 0; i < 5; i++) {
  //     text += possibleText.charAt(
  //       Math.floor(Math.random() * possibleText.length)
  //     );
  //   }
  //   // Replace extension according to your media type
  //   const imageName = date + "." + text + ".jpeg";
  //   // call method that creates a blob from dataUri
  //   var imageBase64 = base64;
  //   var blob = new Blob([imageBase64], { type: "image/png" });
  //   var file = new File([blob], "imageFileName.png");
  //   // const imageBlob = this.dataURItoBlob(base64);
  //   // const imageFile = new File([imageBlob], imageName, { type: "image/jpeg" });
  //   this.profilePicture = file;
  //   console.log(this.profilePicture);
  //   console.log(file);
  // }
  // dataURItoBlob(dataURI) {}
}
