import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  MenuController,
  ToastController,
  Platform
} from "ionic-angular";
import { RegisterPage } from "../register/register";
import { HomePage } from "../home/home";
import { RestProvider } from "../../providers/rest/rest";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { constants, storedConstants } from "../../providers/constants";
import { DataProvider } from "../../providers/data/data";
// import { GooglePlus } from "@ionic-native/google-plus";
// import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";
import { ForgotpasswordPage } from "../forgotpassword/forgotpassword";
// import { TranslateService } from "@ngx-translate/core";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  showVid = false;
  remember = false;
  emailError: boolean;
  school = "";
  passwordError: boolean;
  email = "";
  password = "";
  isFieldError: boolean;
  lang: any;
  device: any;
  device_type: any;
  schools = [];
  schoolError: boolean;
  constructor(
    // private googlePlus: GooglePlus,
    public navCtrl: NavController,
    public menu: MenuController,
    public navParams: NavParams,
    private sharedService: SharedServiceProvider,
    private rest: RestProvider,
    private toastCtrl: ToastController,
    public platform: Platform,
    private dataService: DataProvider, // public translate: TranslateService
    // private fb: Facebook
  ) {
    // this.lang = 'en';
    // this.translate.setDefaultLang('en');
    // this.translate.use('en');
    if (this.platform.is("ios")) {
      this.device_type = 1;
      console.log("I am an iOS device!");
    }
    if (this.platform.is("android")) {
      // This will only print when on Android
      this.device_type = 2;

      console.log("I am an android device!");
    }
  }
  switchLanguage() {
    // this.translate.use(this.lang);
  }
  ionViewWillEnter() { }
  ionViewDidLoad() {
    this.menu.swipeEnable(false);

    if (!this.sharedService.checkForLoginStatus()) {
      this.rest.getDataObservable(constants.SCHOOLLIST).subscribe(schools => {
        if (schools.status) {
          this.schools = schools.data;
          this.sharedService.dismissLoader();
        }
      });
      if (this.dataService.getFromLocalStorage(storedConstants.USER_CRED_CHOICE)) {
        let user = this.dataService.getFromLocalStorage(storedConstants.USER_CRED_CHOICE);
        this.school = user.school;
        this.email = user.email;
        this.password = user.password;
        this.remember = true;
      }
    }
  }
  validation(from) {
    switch (from) {
      case "email":
        this.emailError = false;
        break;
      case "password":
        this.passwordError = false;
        break;
      case "school":
        this.schoolError = false;

        break;
    }
  }
  Login() {
    // if (this.email == "paras@123" && this.password == "paras@123") {
    this.navCtrl.push(HomePage);
    // }
  }
  ionViewDidEnter() {
    // if (!this.dataService.getFromLocalStorage(storedConstants.INTROVIDEO)) {
    //   this.showVid = true;
    // }

    // if (this.showVid) {
    //   setTimeout(() => {
    //     this.showVid = false;
    //     this.dataService.localeStorage(storedConstants.INTROVIDEO, true);
    //   }, 24000);
    // }
    console.log("ionViewDidEnter");
  }
  // skip() {
  //   this.showVid = false;
  //   this.dataService.localeStorage(storedConstants.INTROVIDEO, true);
  // }
  doLogin() {
    // this.mainProvider.getUUID();
    console.log(this.remember);
    if (
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        this.email
      )
    ) {
      this.isFieldError = false;
    } else {
      this.emailError = true;
      this.isFieldError = true;
    }

    if (
      this.password == "" ||
      this.password == undefined ||
      this.password == null
    ) {
      this.passwordError = true;
      this.isFieldError = true;
    } else {
      this.isFieldError = false;
    }

    if (this.school == "" || this.school == undefined || this.school == null) {
      this.isFieldError = true;
      this.schoolError = true;
    } else {
      this.isFieldError = false;
    }

    if (this.emailError || this.passwordError || this.schoolError) {
      return false;
    }

    if (
      this.email == undefined ||
      this.email == "" ||
      this.password == undefined ||
      this.password == ""
    ) {
      let toast = this.toastCtrl.create({
        message: "Please fill all details",
        duration: 2000,
        position: "bottom",
        showCloseButton: true,
        closeButtonText: "ok"
      });
      toast.present();
    } else {
      if (this.school) {
        this.schools.filter(id => {
          if (id.id_school == this.school) {
            console.log(id.urlschool);
            this.rest.BaseUrl = id.urlschool + "/api/";
            this.dataService.localeStorage(storedConstants.DYNAMIC_BASE_URL, id.urlschool + "/api/");
          }
        })
      }
      this.sharedService.presentLoading();
      let input = new FormData();
      input.append("email", this.email);
      input.append("password", this.password);
      input.append("school_id", this.school);
      input.append(
        "device_token",
        this.sharedService.getFcmToken()
          ? this.sharedService.getFcmToken()
          : null
      );
      input.append(
        "device_name",
        this.device ? this.device.model : "No Device Name Found"
      );
      input.append("device_type", this.device_type ? this.device_type : 1);
      // input.append("lat", this.lat);
      // input.append("lon", this.lon);

      this.rest.postDataObservable(constants.LOGIN, input).subscribe(
        data => {
          if (data.status) {
            this.dataService.localeStorage(
              storedConstants.USER_DETAILS,
              JSON.stringify(data)
            );
            if (this.remember) {

              this.dataService.localeStorage(storedConstants.USER_CRED_CHOICE, JSON.stringify({ email: this.email, password: this.password, school: this.school }));
            } else {
              localStorage.removeItem(storedConstants.USER_CRED_CHOICE);
            }
            this.navCtrl.setRoot(HomePage);
            this.sharedService.dismissLoader();

            // this.is_login = true;
          } else {
            this.sharedService.dismissLoader();
            this.sharedService.presentToast(data.error);
          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          console.log(err);
        }
      );
    }
  }
  navToReg() {
    this.navCtrl.push(RegisterPage);
  }
  navToForgotPass() {
    this.navCtrl.push(ForgotpasswordPage);
  }
  // googleLogin() {
  //   if (this.school == "" || this.school == undefined || this.school == null) {
  //     this.schoolError = true;
  //   }
  //   else {
  //     if (this.school) {
  //       this.schools.filter(id => {
  //         if (id.id_school == this.school) {
  //           console.log(id.urlschool);
  //           this.rest.BaseUrl = id.urlschool + "/api/";
  //           this.dataService.localeStorage(storedConstants.DYNAMIC_BASE_URL, id.urlschool + "/api/");
  //         }
  //       })
  //     }
  //     console.log("google login");
  //     this.googlePlus
  //       .login({})
  //       .then(res => {
  //         console.log(res);
  //         if (res.displayName && res.email && res.userId) {
  //           this.socialLogin("Google", res.email, res.displayName, res.userId);
  //         } else {
  //           this.navToReg();
  //         }
  //       })
  //       .catch(err => console.error(err));
  //   }
  // }
  // facebookLogin() {
  //   if (this.school == "" || this.school == undefined || this.school == null) {
  //     this.schoolError = true;
  //   }
  //   else {
  //     if (this.school) {
  //       this.schools.filter(id => {
  //         if (id.id_school == this.school) {
  //           console.log(id.urlschool);
  //           this.rest.BaseUrl = id.urlschool + "/api/";
  //           this.dataService.localeStorage(storedConstants.DYNAMIC_BASE_URL, id.urlschool + "/api/");
  //         }
  //       })
  //     }
  //     this.fb
  //       .login(["public_profile", "email"])
  //       .then((res: FacebookLoginResponse) => {
  //         console.log("Logged into Facebook!", res);
  //         this.getUserDetail(res.authResponse.userID);
  //       })
  //       .catch(e => console.log("Error logging into Facebook", e));
  //   }
  // }
  // getUserDetail(userid) {
  //   this.fb
  //     .api("/" + userid + "/?fields=id,email,name,picture,gender", [
  //       "public_profile"
  //     ])
  //     .then(res => {
  //       console.log(res);
  //       if (res.email && res.name && res.id) {
  //         this.socialLogin("Facebook", res.email, res.name, res.id);
  //       } else {
  //         this.sharedService.presentToast(
  //           "Your Email id is not accessible from facbook please register first"
  //         );
  //         this.navToReg();
  //       }
  //     })
  //     .catch(e => {
  //       console.log(e);
  //     });
  // }
  // socialLogin(type, email, name, id) {
  //   this.sharedService.presentLoading();
  //   let input = new FormData();
  //   input.append("social_type", type);

  //   input.append("email", email);
  //   input.append("prenomp", name);
  //   input.append("social_id", id);
  //   input.append("school_id", this.school);
  //   input.append(
  //     "device_token",
  //     this.sharedService.getFcmToken()
  //       ? this.sharedService.getFcmToken()
  //       : null
  //   );
  //   input.append(
  //     "device_name",
  //     this.device ? this.device.model : "No Device Name Found"
  //   );
  //   input.append("device_type", this.device_type ? this.device_type : 1);
  //   // input.append("lat", this.lat);
  //   // input.append("lon", this.lon);

  //   this.rest.postDataObservable(constants.LOGIN, input).subscribe(
  //     data => {
  //       if (data.status) {
  //         this.sharedService.dismissLoader();
  //         this.dataService.localeStorage(
  //           storedConstants.USER_DETAILS,
  //           JSON.stringify(data)
  //         );
  //         this.navCtrl.setRoot(HomePage);

  //         // this.is_login = true;
  //       } else {
  //         this.sharedService.dismissLoader();
  //         this.sharedService.presentToast(data.error);
  //       }
  //     },
  //     err => {
  //       this.sharedService.presentToast(err.error.message);
  //       this.sharedService.dismissLoader();
  //       console.log(err);
  //     }
  //   );
  // }
}
