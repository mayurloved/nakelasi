import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { constants, storedConstants } from '../../providers/constants';
import { DataProvider } from '../../providers/data/data';
import { SharedServiceProvider } from '../../providers/shared-service/shared-service';
import { Clipboard } from '@ionic-native/clipboard';
/**
 * Generated class for the HistoryTokenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-history-token',
  templateUrl: 'history-token.html',
})
export class HistoryTokenPage implements OnInit {

  historyData: any = [];
  id_titeur: any;
  childData = [];
  showFontSlider = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public rest: RestProvider,
    public dataService: DataProvider,
    public sharedService: SharedServiceProvider,
    public atrCtrl: AlertController,
    private clipboard: Clipboard) {
  }

  ngOnInit(): void {
    this.id_titeur = this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS).data.id_titeur;
    this.getChildren()
    this.fetchHistoryTokenData()
  }

  fetchHistoryTokenData() {
    var data = new FormData();
    data.append("id_titeur", this.id_titeur);
    console.log(this.id_titeur);
    this.sharedService.presentLoading();
    this.rest.postDataObservable(constants.HISTORY_JETON, data).subscribe(detail => {
      this.sharedService.dismissLoader();
      if (detail.status) {
        this.historyData = detail.code_jeton_data
      }
    })
  }

  showChildList() {
    alert('clicked')
    this.showFontSlider = !this.showFontSlider;
  }

  getChildren() {
    if (this.sharedService.checkForLoginStatus()) {
      this.sharedService.presentLoading();

      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      ).data.id_titeur;
      let input = new FormData();
      input.append("id_titeur", this.id_titeur);
      this.rest
        .postDataObservable(constants.CHILDRENLIST, input)
        .subscribe(
          res => {
            if (res.status) {
              this.childData = res.child_data;
              // this.cards = res.news_data;
              this.sharedService.dismissLoader();
            } else {
              this.sharedService.dismissLoader();
            }
          },
          err => {
            this.sharedService.presentToast(err.error.message);
            this.sharedService.dismissLoader();
            console.log(err);
          }
        );
    }
  }

  showRadioAlert(token) {
    console.log(token)
    let showAlert = this.atrCtrl.create();
    showAlert.setTitle('choisir un enfant');

    this.childData.forEach(element => {
      showAlert.addInput({
        type: 'radio',
        label: element.prenom + ' ' + element.nom,
        value: element.id_eleve,
      });
    });
    showAlert.addButton('Cancel');
    showAlert.addButton({
      text: 'OK',
      handler: id => {
        if (id) {
          this.jatonAbonnementClick(token, id)
        } else {
          alert('Enfant invalide')
        }
      }
    });
    showAlert.present();
  }

  jatonAbonnementClick(token, id) {
    this.sharedService.presentLoading();
    var data = new FormData();
    data.append("id_eleve", id);
    data.append("code_jeton", token);
    this.rest
      .postDataObservable(constants.HISTORY_JETON, data)
      .subscribe((detail) => {
        console.log(data);
        if (detail.status) {
          this.fetchHistoryTokenData()
          this.sharedService.dismissLoader();
          this.sharedService.presentToast(detail.message);
        } else {
          this.sharedService.dismissLoader();
          this.sharedService.presentToast(detail.message);
        }
      });
  }

  // copyJeton(jeton) {
  //   this.clipboard.copy(jeton);
  //   this.sharedService.presentToast('Code Copied')
  // }

}
