import { NgModule } from '@angular/core';
import { SearchPipe } from './search/search';
import { SlugifyPipe } from './slugify/slugify';
@NgModule({
	declarations: [SearchPipe,SlugifyPipe],
	imports: [],
	exports: [SearchPipe, SlugifyPipe]
})
export class PipesModule {}
