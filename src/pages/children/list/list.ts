import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { DetailPage } from "../detail/detail";
import { DataProvider } from "../../../providers/data/data";
import { SharedServiceProvider } from "../../../providers/shared-service/shared-service";
import { storedConstants, constants } from "../../../providers/constants";
import { RestProvider } from "../../../providers/rest/rest";

@Component({
  selector: "page-list",
  templateUrl: "list.html"
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items = [];
  id_titeur: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public sharedService: SharedServiceProvider,
    public restService: RestProvider,
    public dataService: DataProvider
  ) {
    // If we navigated to this page, we will have an item available as a nav param
    console.log(navParams);
    this.items = [];
    this.getChildren();
    // this.selectedItem = navParams.get("item");

    // Let's populate this page with some filler content for funzies
    this.icons = [
      "flask",
      "wifi",
      "beer",
      "football",
      "basketball",
      "paper-plane",
      "american-football",
      "boat",
      "bluetooth",
      "build"
    ];

    // for (let i = 1; i < 11; i++) {
    //   this.items.push({
    //     title: "Item " + i,
    //     note: "This is item #" + i,
    //     icon: this.icons[Math.floor(Math.random() * this.icons.length)]
    //   });
    // }
  }
  getChildren() {
    if (this.sharedService.checkForLoginStatus()) {
      this.sharedService.presentLoading();

      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      ).data.id_titeur;
      let input = new FormData();
      input.append("id_titeur", this.id_titeur);
      this.restService
        .postDataObservable(constants.CHILDRENLIST, input)
        .subscribe(
          res => {
            if (res.status) {
              this.items = res.child_data;
              // this.cards = res.news_data;
              this.sharedService.dismissLoader();
            } else {
              this.sharedService.dismissLoader();
            }
          },
          err => {
            this.sharedService.presentToast(err.error.message);
            this.sharedService.dismissLoader();
            console.log(err);
          }
        );
    }
  }
  // itemTapped(event, item) {
  //   // That's right, we're pushing to ourselves!
  //   this.navCtrl.push(ListPage, {
  //     item: item
  //   });
  // }

  goToDetail(item) {
    this.navCtrl.push(DetailPage, item);
  }
  doRefresh(refresher) {
    console.log(refresher);
    console.log("Begin async operation", refresher);
    this.getChildren();
    setTimeout(() => {
      console.log("Async operation has ended");
      refresher.complete();
    }, 2000);
  }
}
