import { Component } from "@angular/core";
import { NavController, Events } from "ionic-angular";
import { NewsDetailPage } from "../news-detail/news-detail";
import { constants, storedConstants } from "../../providers/constants";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { DataProvider } from "../../providers/data/data";
import { DetailPage } from "../children/detail/detail";
import { RestProvider } from "../../providers/rest/rest";
import { SocialSharing } from "@ionic-native/social-sharing";

@Component({
  selector: "page-home",
  templateUrl: "home.html",
})
export class HomePage {
  showVid = false;
  fev = true;
  cards = [];
  enBref = [];
  id_titeur: any;
  childrenData = [];
  randomArray: any;
  nbn = 0;
  count = 1;
  showSearch = false;
  terms = "";
  nomschool = "";

  tabcategory: any = "";
  // share_count:any = 0;

  cat_count: any = 0;

  constructor(
    public navCtrl: NavController,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider,
    public events: Events,
    private socialSharing: SocialSharing
  ) {
    events.subscribe("nbn", (data) => {
      this.nbn = data > 0 ? data : 0;
    });
  }
  ionViewWillEnter() {
    this.count = 1;
    var element = document.getElementById("myDIV");
    element.classList.remove("has-refresher");
    // if (this.sharedService.checkForLoginStatus()) {
    this.sharedService.presentLoading();
    this.id_titeur = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    )
      ? this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS).data
          .id_titeur
      : "";
    this.nomschool = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    )
      ? this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS)
          .school_info.nomschool
      : "";
    let input = new FormData();
    if (this.id_titeur) {
      input.append("id_titeur", this.id_titeur);
    }
    if (this.terms && this.terms.length > 0 && this.terms != "") {
      input.append("search_key", this.terms);
    } else {
      input.append("page_count", "1");
    }
    this.restService.postDataObservable(constants.HOME, input).subscribe(
      (res) => {
        if (res.status) {
          this.count++;
          this.childrenData = res.child_data;
          this.cards = res.news_data.filter((id) => {
            id.created = id.created.replace(/\s/g, "T");
            return id;
          });
          this.events.publish("reception_count", res.reception_count);
          this.events.publish("envoyes_count", res.envoyes_count);
          this.events.publish("corbeille_count", res.corbeille_count);
          this.events.publish("nbn", res.nbn);
          this.events.publish(
            "notification_msg",
            res.notification_msg.toString()
          );
          this.events.publish("notification_count", res.notification_count);
          this.nbn = res.nbn;
          this.sharedService.dismissLoader();
        } else {
          this.sharedService.presentToast(res.message);
          this.sharedService.dismissLoader();
        }
      },
      (err) => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        console.log(err);
      }
    );
    // }
  }

  favChange(newsId) {
    console.log(newsId);
    if (this.sharedService.checkForLoginStatus() && newsId) {
      this.sharedService.presentLoading();
      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      )
        ? this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS)
            .data.id_titeur
        : "";
      let input = new FormData();
      input.append("titeur_id", this.id_titeur);
      input.append("news_id", newsId.id_news);
      this.restService
        .postDataObservable(constants.UPDATEFAVOURITE, input)
        .subscribe(
          (res) => {
            if (res.status) {
              newsId.favorite = newsId.favorite == 1 ? 0 : 1;
              this.sharedService.presentToast(res.message);
              this.sharedService.dismissLoader();
            } else {
              this.sharedService.presentToast(res.message);

              this.sharedService.presentToast(res.message);
              this.sharedService.dismissLoader();
            }
          },
          (err) => {
            this.sharedService.presentToast(err.error.message);
            this.sharedService.dismissLoader();

            console.log(err);
          }
        );
    }
  }
  // skip() {
  //   this.showVid = false;
  //   this.dataService.localeStorage(storedConstants.INTROVIDEO, true);
  // }
  ionViewDidEnter() {
    // if (!this.dataService.getFromLocalStorage(storedConstants.INTROVIDEO)) {
    //   this.showVid = true;
    // }

    // if (this.showVid) {
    //   setTimeout(() => {
    //     this.showVid = false;
    //     this.dataService.localeStorage(storedConstants.INTROVIDEO, true);
    //   }, 24000);
    // }
    console.log("ionViewDidEnter");
  }
  navToChildDetail(i) {
    this.navCtrl.push(DetailPage, i);
  }
  doInfinite(infiniteScroll) {
    if (this.tabcategory != "") {
      console.log("Begin async operation");
      // setTimeout(() => {
      // if (this.sharedService.checkForLoginStatus()) {
      this.showSearch = false;
      this.terms = "";
      // this.sharedService.presentLoading();
      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      )
        ? this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS)
            .data.id_titeur
        : "";
      let input = new FormData();
      if (this.id_titeur) {
        input.append("id_titeur", this.id_titeur);
      }
      // input.append("page_count_type", this.cat_count.toString());
      // input.append("news_type", this.tabcategory);

      if (this.tabcategory !== "en Bref") {
        input.append("page_count_type", this.cat_count);
        input.append("news_type", this.tabcategory);
      } else {
        input.append("page_count", this.cat_count);
      }

      this.restService.postDataObservable(constants.HOME, input).subscribe(
        (res) => {
          if (res.status) {
            this.cat_count++;

            // this.childrenData = res.child_data;
            let cards = res.news_data.filter((id) => {
              id.created = id.created.replace(/\s/g, "T");
              return id;
            });
            if (res.enbrief_data) {
              let enBref = res.enbrief_data;
              // let enBref = res.enbrief_data.filter(id => {
              //   id.date_breve = id.date_breve.replace(/\s/g, "T");
              //   return id;
              // });
              if (enBref.length > 0) {
                for (let i = 0; i < enBref.length; i++) {
                  this.enBref.push(enBref[i]);
                }
              }
            }
            if (cards.length > 0) {
              for (let i = 0; i < cards.length; i++) {
                this.cards.push(cards[i]);
              }
            }
            this.events.publish("reception_count", res.reception_count);
            this.events.publish("envoyes_count", res.envoyes_count);
            this.events.publish("corbeille_count", res.corbeille_count);
            this.events.publish("nbn", res.nbn);
            this.events.publish(
              "notification_msg",
              res.notification_msg.toString()
            );
            this.events.publish("notification_count", res.notification_count);
            this.nbn = res.nbn;
            this.sharedService.dismissLoader();
            infiniteScroll.complete();
          } else {
            this.sharedService.presentToast(res.message);
            this.sharedService.dismissLoader();
            infiniteScroll.complete();
          }
        },
        (err) => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          infiniteScroll.complete();
          console.log(err);
        }
      );
    } else {
      console.log("Begin async operation");
      // setTimeout(() => {
      // if (this.sharedService.checkForLoginStatus()) {
      this.showSearch = false;
      this.terms = "";
      // this.sharedService.presentLoading();
      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      )
        ? this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS)
            .data.id_titeur
        : "";
      let input = new FormData();
      if (this.id_titeur) {
        input.append("id_titeur", this.id_titeur);
      }
      input.append("page_count", this.count.toString());
      this.restService.postDataObservable(constants.HOME, input).subscribe(
        (res) => {
          if (res.status) {
            this.count++;

            // this.childrenData = res.child_data;
            let cards = res.news_data.filter((id) => {
              id.created = id.created.replace(/\s/g, "T");
              return id;
            });
            if (cards.length > 0) {
              for (let i = 0; i < cards.length; i++) {
                this.cards.push(cards[i]);
              }
            }
            this.events.publish("reception_count", res.reception_count);
            this.events.publish("envoyes_count", res.envoyes_count);
            this.events.publish("corbeille_count", res.corbeille_count);
            this.events.publish("nbn", res.nbn);
            this.events.publish(
              "notification_msg",
              res.notification_msg.toString()
            );
            this.events.publish("notification_count", res.notification_count);
            this.nbn = res.nbn;
            this.sharedService.dismissLoader();
            infiniteScroll.complete();
          } else {
            this.sharedService.presentToast(res.message);
            this.sharedService.dismissLoader();
            infiniteScroll.complete();
          }
        },
        (err) => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          infiniteScroll.complete();
          console.log(err);
        }
      );
    }

    // }
    // }, 1000);
  }

  gotoNewsDetail(main) {
    this.randomGenerator(3, this.cards);
    if (this.randomArray) {
      this.navCtrl.push(NewsDetailPage, {
        main: main,
        // other: [this.cards[i + 1], this.cards[i + 2], this.cards[i + 3]]
        other: this.randomArray,
        cards: this.cards,
      });
    }
  }
  randomGenerator(i, array) {
    let card = [];
    for (let j = 0; j < i; j++) {
      card.push(array[Math.floor(Math.random() * array.length)]);
    }
    console.log(card);
    return this.checkFORUnique(card, i);
  }
  checkFORUnique(array, len) {
    var result = array
      .map((item) => item.id_news)
      .filter((value, index, self) => self.indexOf(value) === index);
    console.log(array);
    if (result.length !== len) {
      this.randomGenerator(len, this.cards);
      // return false;
    } else {
      this.randomArray = array;
    }
  }
  doRefresh(refresher) {
    if (this.tabcategory != "") {
      let e = [];
      e["value"] = this.tabcategory;
      console.log(e);
      this.segmentChanged(e);
      setTimeout(() => {
        console.log("Async operation has ended");
        refresher.complete();
      }, 2000);
    } else {
      console.log(refresher);
      console.log("Begin async operation", refresher);
      this.showSearch = false;
      this.terms = "";

      this.ionViewWillEnter();
      setTimeout(() => {
        console.log("Async operation has ended");
        refresher.complete();
      }, 2000);
    }
  }
  ionChange() {
    console.log(this.terms);

    this.count = 1;

    // if (this.sharedService.checkForLoginStatus()) {

    this.id_titeur = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    )
      ? this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS).data
          .id_titeur
      : "";
    this.nomschool = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    )
      ? this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS)
          .school_info.nomschool
      : "";
    let input = new FormData();
    if (this.id_titeur) {
      input.append("id_titeur", this.id_titeur);
    }
    if (this.terms && this.terms.length > 0) {
      input.append("search_key", this.terms);
    } else {
      input.append("page_count", "1");
    }
    if (this.tabcategory != "") {
      input.append("news_type", this.tabcategory);
    }
    this.restService.postDataObservable(constants.HOME, input).subscribe(
      (res) => {
        if (res.status) {
          // this.count++;
          // this.childrenData = res.child_data;
          this.cards = res.news_data.filter((id) => {
            id.created = id.created.replace(/\s/g, "T");
            return id;
          });
          this.events.publish("reception_count", res.reception_count);
          this.events.publish("envoyes_count", res.envoyes_count);
          this.events.publish("corbeille_count", res.corbeille_count);
          this.events.publish("nbn", res.nbn);
          this.events.publish(
            "notification_msg",
            res.notification_msg.toString()
          );
          this.events.publish("notification_count", res.notification_count);
          this.nbn = res.nbn;
        } else {
          this.sharedService.presentToast(res.message);
        }
      },
      (err) => {
        this.sharedService.presentToast(err.error.message);

        console.log(err);
      }
    );
    // }
  }
  // getImagePath(imgs, type) {
  //   console.log(imgs);
  //   switch (type) {
  //     case "child":
  //       return "http://ss.nakelasi.com/members/eleves/" + imgs;

  //     case "news":
  //       return "http://ss.nakelasi.com/members/eleves/" + imgs;
  //   }
  // }

  share(news, share_link) {
    event.stopPropagation();

    this.sharedService.presentLoading();

    this.socialSharing
      .share(share_link)
      .then((res) => {
        let input = new FormData();
        input.append("news_id", news.id_news);
        this.restService
          .postDataObservable(constants.SHARE_COUNT, input)
          .subscribe(
            (res) => {
              this.ionViewWillEnter();
            },
            (err) => {}
          );
        this.sharedService.dismissLoader();
      })
      .catch((err) => {
        this.sharedService.dismissLoader();
      });
  }

  //segement change event
  segmentChanged(e) {
    this.cat_count = 1;
    this.tabcategory = e.value;
    this.id_titeur = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    )
      ? this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS).data
          .id_titeur
      : "";
    this.nomschool = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    )
      ? this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS)
          .school_info.nomschool
      : "";
    let input = new FormData();
    if (this.id_titeur) {
      input.append("id_titeur", this.id_titeur);
    }
    if (e.value !== "en Bref") {
      input.append("page_count_type", this.cat_count);
      input.append("news_type", e.value);
    } else {
      input.append("page_count", this.cat_count);
    }
    this.sharedService.presentLoading();
    this.restService.postDataObservable(constants.HOME, input).subscribe(
      (res) => {
        this.cat_count++;
        this.cards = res.news_data.filter((id) => {
          id.created = id.created.replace(/\s/g, "T");
          return id;
        });
        if (res.enbrief_data) {
          this.enBref = res.enbrief_data;
          // this.enBref = res.enbrief_data.filter(id => {
          //   id.date_breve = id.date_breve.replace(/\s/g, "T");
          //   return id;
          // });
        }
        this.events.publish("reception_count", res.reception_count);
        this.events.publish("envoyes_count", res.envoyes_count);
        this.events.publish("corbeille_count", res.corbeille_count);
        this.events.publish("nbn", res.nbn);
        this.events.publish(
          "notification_msg",
          res.notification_msg.toString()
        );
        this.events.publish("notification_count", res.notification_count);
        this.nbn = res.nbn;
        this.sharedService.dismissLoader();
      },
      (err) => {
        this.sharedService.dismissLoader();
      }
    );
  }
}
