import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { ReleaseDetailPage } from "../release-detail/release-detail";
import { DataProvider } from "../../../providers/data/data";
import { SharedServiceProvider } from "../../../providers/shared-service/shared-service";
import { RestProvider } from "../../../providers/rest/rest";
import { constants } from "../../../providers/constants";

/**
 * Generated class for the SchoolNewsReleasesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-school-news-releases",
  templateUrl: "school-news-releases.html"
})
export class SchoolNewsReleasesPage {
  items = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider
  ) {
    this.getNewRelease();
  }
  getNewRelease() {
    if (this.sharedService.checkForLoginStatus()) {
      this.sharedService.presentLoading();

      let input = new FormData();

      this.restService
        .postDataObservable(constants.COMMUNIQUES, input)
        .subscribe(
          res => {
            if (res.status) {
              console.log(res);
              this.items = res.data_communiques.filter(id => {
                id.created = id.created.replace(/\s/g, "T");
                return id;
              });

              // this.cards = res.news_data;
              this.sharedService.dismissLoader();
            } else {
              this.sharedService.dismissLoader();
            }
          },
          error => {
            this.sharedService.presentToast(error["error"].message);
            this.sharedService.dismissLoader();
          }
        );
    }
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad SchoolNewsReleasesPage");
  }
  navToNewsDetail(item) {
    this.navCtrl.push(ReleaseDetailPage, item);
  }
}
