import { Component } from "@angular/core";
import { NavController, NavParams, ViewController } from "ionic-angular";
import { DataProvider } from "../../../providers/data/data";
import { SharedServiceProvider } from "../../../providers/shared-service/shared-service";
import { RestProvider } from "../../../providers/rest/rest";
import { constants } from "../../../providers/constants";

/**
 * Generated class for the AddeventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-addevent",
  templateUrl: "addevent.html"
})
export class AddeventPage {
  eventObj = { id_titeur: "", title: "", start: "", end: "", id: "" };
  editMode: boolean;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtr: ViewController,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider
  ) {
    console.log(navParams.data);
    if (navParams.data) {
      const data = this.navParams.data;
      if (data.id) {
        this.editMode = true;
      }
      this.eventObj.id = data.id ? data.id : null;
      this.eventObj.title = data.title ? data.title : "";
      this.eventObj.start = data.start ? data.start : "";
      this.eventObj.end = data.end ? data.end : null;
      this.eventObj.id_titeur = data.id_titeur ? data.id_titeur : "";
    }
  }
  close() {
    this.viewCtr.dismiss();
  }
  addEvent() {
    if (this.eventObj.title && this.eventObj.start) {
      if (this.sharedService.checkForLoginStatus()) {
        this.sharedService.presentLoading();
        let input = new FormData();
        input.append("id_titeur", this.eventObj.id_titeur);
        input.append("title", this.eventObj.title);
        input.append("startdate", this.eventObj.start);
        input.append("enddate", this.eventObj.end);
        this.restService
          .postDataObservable(constants.CALENDAR, input)
          .subscribe(
            data => {
              if (data.status) {
                console.log(data);
                this.sharedService.presentToast(data.message);
                this.close();
                this.sharedService.dismissLoader();
              } else {
                this.sharedService.dismissLoader();
                this.close();
              }
            },
            err => {
              this.sharedService.presentToast(err.error.message);
              this.sharedService.dismissLoader();
              this.close();

              console.log(err);
            }
          );
      }
    } else {
      this.sharedService.presentToast("Title and Start date is required");
    }
  }
  updateEvent() {
    if (this.eventObj.title && this.eventObj.start && this.editMode) {
      if (this.sharedService.checkForLoginStatus()) {
        this.sharedService.presentLoading();

        let input = new FormData();
        input.append("id_titeur", this.eventObj.id_titeur);
        input.append("event_id", this.eventObj.id);
        input.append("title", this.eventObj.title);
        input.append("startdate", this.eventObj.start);
        input.append("enddate", this.eventObj.end);
        this.restService
          .postDataObservable(constants.CALENDAR, input)
          .subscribe(
            data => {
              if (data.status) {
                console.log(data);
                this.sharedService.presentToast(data.message);
                this.close();
                this.sharedService.dismissLoader();
              } else {
                this.sharedService.dismissLoader();
                this.close();
              }
            },
            err => {
              this.sharedService.presentToast(err.error.message);
              this.sharedService.dismissLoader();
              console.log(err);
              this.close();
            }
          );
      }
    } else {
      this.sharedService.presentToast("Title and Start date is required");
    }
  }
  deleteEvent() {
    if (this.editMode) {
      if (this.sharedService.checkForLoginStatus()) {
        this.sharedService.presentLoading();

        let input = new FormData();
        input.append("id_titeur", this.eventObj.id_titeur);
        input.append("event_id_delete", this.eventObj.id);

        this.restService
          .postDataObservable(constants.CALENDAR, input)
          .subscribe(
            data => {
              if (data.status) {
                console.log(data);
                this.sharedService.presentToast(data.message);
                this.close();
                this.sharedService.dismissLoader();
              } else {
                this.sharedService.dismissLoader();
                this.close();
              }
            },
            err => {
              this.sharedService.presentToast(err.error.message);
              this.sharedService.dismissLoader();
              this.close();

              console.log(err);
            }
          );
      }
    } else {
      this.sharedService.presentToast("Title and Start date is required");
    }
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad AddeventPage");
  }
}
