import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { constants, storedConstants } from "../../providers/constants";
import { DataProvider } from "../../providers/data/data";
/**
 * Generated class for the ParrainagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-apropos",
  templateUrl: "apropos.html"
})
export class AproposPage {
 
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public rest: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider
  ) {}

  ionViewWillLoad() {
    
  }
  
}
