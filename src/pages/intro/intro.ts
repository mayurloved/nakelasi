import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { SharedServiceProvider } from '../../providers/shared-service/shared-service';
import { storedConstants } from '../../providers/constants';
import { DataProvider } from '../../providers/data/data';

/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

  constructor(public navCtrl: NavController, public dataService: DataProvider, public sharedService: SharedServiceProvider) {

  }

  goToHome() {
    this.dataService.localeStorage(storedConstants.INTRO, true);
    if (this.sharedService.checkForLoginStatus()) {
      this.navCtrl.setRoot(HomePage);

    } else {

      this.navCtrl.setRoot(LoginPage);
    }
  }

}
