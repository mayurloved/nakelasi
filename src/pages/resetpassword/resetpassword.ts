import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { RestProvider } from "../../providers/rest/rest";
import { constants, storedConstants } from "../../providers/constants";
import { DataProvider } from "../../providers/data/data";

/**
 * Generated class for the ResetpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-resetpassword",
  templateUrl: "resetpassword.html"
})
export class ResetpasswordPage {
  confirmpasswordError = false;
  confirmpassword: any;
  password: any;
  passwordError: boolean;
  isFieldError: boolean;
  lengthError: boolean;
  id_email: any;
  paramData: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public sharedService: SharedServiceProvider,
    public rest: RestProvider,
    public dataService: DataProvider
  ) {
    this.paramData = navParams.data;
    if (this.paramData.email) {
      this.id_email = this.paramData.email;
    }
    if (sharedService.checkForLoginStatus()) {
      this.id_email = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      ).data.email;
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ResetpasswordPage");
  }
  validation(from) {
    switch (from) {
      case "confirmpassword":
        this.confirmpasswordError = false;
        break;

      case "password":
        this.passwordError = false;
        break;
    }
  }
  doRegistraton() {
    if (!this.confirmpassword || this.confirmpassword != this.password) {
      this.confirmpasswordError = true;
      this.isFieldError = true;
    } else {
      if (this.password.length < 7) {
        this.isFieldError = true;
        this.lengthError = true;
      } else {
        this.lengthError = false;

        this.isFieldError = false;
      }
    }

    if (
      this.password == "" ||
      this.password == undefined ||
      this.password == null
    ) {
      this.passwordError = true;
      this.isFieldError = true;
    } else {
      this.isFieldError = false;
    }

    if (this.confirmpasswordError || this.passwordError || this.lengthError) {
      return false;
    }

    this.sharedService.presentLoading();
    let input = new FormData();
    if (this.sharedService.checkForLoginStatus()) {
      input.append("reset_password", this.password);
    } else {
      input.append("password", this.password);
    }
    // input.append("mobile", this.phno);
    input.append("email", this.id_email);

    // input.append("gender", this.gender);
    // input.append("dob", this.dob);
    this.rest.postDataObservable(constants.FORGOTPASSWORD, input).subscribe(
      data => {
        if (data.status) {
          this.sharedService.dismissLoader();
          this.sharedService.presentToast(data.message);
          this.navCtrl.popToRoot();
        } else {
          // this.mainProvider.hideLoading();
          this.sharedService.dismissLoader();

          this.sharedService.presentToast(data.error[0]);
        }
      },
      err => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        console.log(err);
      }
    );
    // }
  }
  Cancel() {
    this.navCtrl.pop();
    this.password = "";
    this.confirmpassword = "";
  }
}
