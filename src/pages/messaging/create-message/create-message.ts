import { Component } from "@angular/core";
import { NavController, NavParams, AlertController } from "ionic-angular";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { DataProvider } from "../../../providers/data/data";
import { SharedServiceProvider } from "../../../providers/shared-service/shared-service";
import { RestProvider } from "../../../providers/rest/rest";
import { storedConstants, constants } from "../../../providers/constants";
import { MessagingPage } from "../messaging";

/**
 * Generated class for the CreateMessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-create-message",
  templateUrl: "create-message.html"
})
export class CreateMessagePage {
  // message: FormGroup;
  text = "";
  id_titeur: any;
  Tempmessage = "";
  categorie;
  destinataire;
  objet;
  Tempobjet: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public alertCtrl: AlertController,

    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider
  ) {
    // this.message = this.formBuilder.group({
    //   // categorie: ["", Validators.required],
    //   // objet: ["", Validators.required],
    //   // text: ["", Validators.required],
    this.destinataire = "ladirection";
    // });
    console.log(navParams.data);
    if (navParams.data) {
      // this.message.patchValue(this.navParams.data);
      this.text = this.navParams.data.text;
      this.Tempmessage = this.text;

      this.categorie = this.navParams.data.categorie;
      this.objet = this.navParams.data.objet;
      this.Tempobjet = this.objet;
      // this.text = this.navParams.data.text;

      console.log(navParams.data);
      if (navParams.data.type == "Tr") {
        this.destinataire = "";
      }
    }
  }

  ionViewWillEnter() {
    this.destinataire = "ladirection";
    if (this.navParams.data.type == "Tr") {
      this.destinataire = "";
    }
  }
  createMsg() {
    // this.message.controls.text.setValue(this.text);

    console.log(this.destinataire);
    console.log(this.Tempobjet);
    console.log(this.categorie);
    console.log(this.Tempmessage);
    if (this.sharedService.checkForLoginStatus()) {
      // this.message.controls.text.setValue(this.Tempmessage);
      if (
        this.destinataire &&
        this.Tempobjet &&
        this.categorie &&
        this.Tempmessage
      ) {
        // let data = this.message.value;
        this.sharedService.presentLoading();

        this.id_titeur = this.dataService.getFromLocalStorage(
          storedConstants.USER_DETAILS
        ).data.id_titeur;
        let input = new FormData();
        input.append("id_titeur", this.id_titeur);
        input.append("categorie", this.categorie);
        input.append("destinataire", this.destinataire);
        input.append("objet", this.Tempobjet);
        input.append("text", this.Tempmessage);
        this.restService
          .postDataObservable(constants.COMPOSE_MAIL, input)
          .subscribe(
            res => {
              console.log(res);
              if (res && res["status"]) {
                // this.sharedService.presentToast(res["message"]);
                // this.result = res;
                // this.items = res.inbox_mails;
                // this.cards = res.news_data;

                this.sharedService.dismissLoader();
                let prompt = this.alertCtrl.create({
                  title: "Nakelasi",
                  message: res["message"],
                  cssClass: "alert-success",
                  buttons: [
                    {
                      text: "OK",
                      handler: data => {
                        this.navCtrl.setRoot(MessagingPage, "sentMessage");
                        // this.menu.open();
                        // setTimeout(() => {
                        // 	this.menu.close();
                        // }, 800);
                        // this.mainProvider.hideLoading();
                      }
                    }
                  ]
                });
                prompt.present();
              } else {
                this.sharedService.presentToast(res["message"]);

                this.sharedService.dismissLoader();
              }
            },
            error => {
              this.sharedService.presentToast(error["error"].message);
              this.sharedService.dismissLoader();
            } // error path
          );
        // .catch(err => {
        //   this.sharedService.presentToast(err["error"].message);
        //   this.sharedService.dismissLoader();
        // });
      } else {
        this.sharedService.presentToast(
          "Erreur veuillez completer tous les champs avant d'envoyer votre message"
        );
      }
    }
  }

  setText(text, type) {
    if (type == "text") {
      this.Tempmessage = text;
      console.log(text + "---" + type);
      console.log(this.Tempmessage);
    } else if (type == "objet") {
      this.Tempobjet = text;
    }
  }
}
