import { Component, ViewChild } from "@angular/core";
import { NavController, NavParams, Content } from "ionic-angular";
import { DataProvider } from "../../providers/data/data";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { RestProvider } from "../../providers/rest/rest";
import { constants, storedConstants } from "../../providers/constants";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
/**
 * Generated class for the NewsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 * https://medium.com/@AAlakkad/angular-2-display-html-without-sanitizing-filtering-17499024b079
 */

@Component({
  selector: "page-supports",
  templateUrl: "supports.html"
})
export class Supports {
  showDetail :boolean = false;
  id_titeur: any;
  support_list: any;
  subject: any;
  desc: any;
  show_hide_modal: boolean = false;
  @ViewChild(Content) content: Content;

  constructor(
    public navCtrl: NavController,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider,
    public iab: InAppBrowser
  ) {}

  ionViewWillEnter() {
    this.getSupportList();
  }

  getSupportList() {
    this.id_titeur = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    ).data.id_titeur;
    let input = new FormData();
    input.append("id_titeur_list", this.id_titeur);

    this.restService
      .postDataObservable(constants.LIST_SUPPORTS, input)
      .subscribe(res => {
        if (res.status) {
          this.support_list = res.faq_data;
        }
      });
  }

  openWebsiteUrl(url) {
    this.iab.create(url, "_system");
  }

  showHideModal() {
    this.show_hide_modal = !this.show_hide_modal;
  }

  sendData() {
    if (
      this.subject == "" ||
      this.subject == undefined ||
      this.desc == "" ||
      this.desc == undefined
    ) {
      this.sharedService.presentToast("Veuillez compléter tous les champs");
      return false;
    }
    this.id_titeur = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    ).data.id_titeur;
    let input = new FormData();
    input.append("id_titeur", this.id_titeur);
    input.append("sujet", this.subject);
    input.append("descript", this.desc);
    this.restService
      .postDataObservable(constants.LIST_SUPPORTS, input)
      .subscribe(
        res => {
          if (res.status) {
            this.sharedService.presentToast(res.message);
            this.desc = "";
            this.subject = "";
            this.show_hide_modal = false;
          } else {
            this.sharedService.presentToast(res.message);
          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();
          console.log(err);
        }
      );
  }

  clearData() {
    this.desc = "";
    this.subject = "";
    this.show_hide_modal = false;
  }
}
