import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
// import { Network } from 'ionic-native';
import { Network } from '@ionic-native/network';
import { Observable } from 'rxjs/Observable';
/*
  Generated class for the ConnectivityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConnectivityProvider {

  onDevice: boolean;
 
  constructor(public platform: Platform , public Network :Network){
    this.onDevice = this.platform.is('cordova');
  }
 
  isOnline(): boolean {
    if(this.onDevice && this.Network.type != "none"  ){
      return true;
    } else {
      return navigator.onLine;
    }
  }
 
  isOffline(): boolean {
    if(this.onDevice && this.Network.type == "none"  ){
      return true;
    } else {
      return !navigator.onLine;  
    }
  }
 
  watchOnline(): Observable<any> {
    return this.Network.onConnect();
  }
 
  watchOffline(): Observable<any> {
    return this.Network.onDisconnect();
  }
}
