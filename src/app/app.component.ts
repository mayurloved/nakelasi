import { Component, ViewChild, OnInit } from "@angular/core";
import { Nav, Platform, MenuController, Events } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import "rxjs/add/operator/map";
import { HomePage } from "../pages/home/home";

import { LoginPage } from "../pages/login/login";
import { MessagingPage } from "../pages/messaging/messaging";
import { SharedServiceProvider } from "../providers/shared-service/shared-service";
import { storedConstants } from "../providers/constants";
import { DataProvider } from "../providers/data/data";
import { ConnectivityProvider } from "../providers/connectivity/connectivity";
import { ListPage } from "../pages/children/list/list";
import { SchoolNewsReleasesPage } from "../pages/messaging/school-news-releases/school-news-releases";
import { CreateMessagePage } from "../pages/messaging/create-message/create-message";
import { AboutSchoolPage } from "../pages/children/about-school/about-school";
import { UserprofilePage } from "../pages/userprofile/userprofile";
import { Partenaires } from "../pages/partenaires/partenaires";
import { AboutEcole } from "../pages/aboutEcole/aboutEcole";
import { ParrainagesPage } from "../pages/parrainages/parrainages";
import { Temoignages } from "../pages/temoignages/temoignages";
import { Supports } from "../pages/supports/supports";
import { StatistiquesPage } from "../pages/statistiques/statistiques";
import { CalendarPage } from "../pages/calendar/calendar";
import { AproposPage } from "../pages/apropos/apropos";
import { FavorisPage } from "../pages/favoris/favoris";
import { NotificationDisplayPage } from "../pages/notification-display/notification-display";
import { RestProvider } from "../providers/rest/rest";
import { AppVersion } from "@ionic-native/app-version";
import { IntroPage } from "../pages/intro/intro";
import { LegalTextsPage } from "../pages/legalTexts/legalTexts";
import { HistoryTokenPage } from "../pages/history-token/history-token";
import {EnPage} from '../pages/en/en';
@Component({
  templateUrl: "app.html",
})
export class MyApp implements OnInit {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{ title: string; component: any }>;
  information: any[];
  user: any;
  profile: any;
  reception_count: any;
  envoyes_count: any;
  information2: any[];
  notificationListCount: number = 0;
  notificationListName = "";
  nbn = 0;
  applicationVersion: string;

  PUSHWOOSH_APP_ID: string = "00E87-73724"; // Your pushwoosh app id.
  GOOGLE_PROJECT_NUMBER: string = "777189002964"; // Project number from firebase.
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public menu: MenuController,
    public sharedService: SharedServiceProvider,
    public events: Events,
    public dataService: DataProvider,
    public connectivity: ConnectivityProvider,
    private rest: RestProvider,
    private appVersion: AppVersion
  ) {
    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: "Home", component: HomePage },
      { title: "Communication", component: ListPage },
      { title: "Events", component: ListPage },
      { title: "Parents meeting", component: ListPage },
    ];
    events.subscribe("session:expired", () => {
      this.rootPage = LoginPage;
    });
    events.subscribe("NotificationPage", (event) => {
      this.nav.push(NotificationDisplayPage, event);
    });
    events.subscribe("Notification:Token", (token) => {
      console.log("received Token------>", token);
      // alert(token)
      this.sharedService.resgistrationObj = token;
    });
    events.subscribe("reception_count", (data) => {
      this.reception_count = data;
      console.log(data);
    });
    events.subscribe("envoyes_count", (data) => {
      this.envoyes_count = data;
      console.log(data);
    });

    events.subscribe("notification_msg", (data) => {
      this.notificationListName = data;
      console.log("notification name..", data);
    });

    events.subscribe("notification_count", (data) => {
      this.notificationListCount = data;
      console.log(data);
    });
    events.subscribe("nbn", (data) => {
      this.nbn = data > 0 ? data : 0;
    });

    events.subscribe("profile_update", (data) => {
      console.log(data);
      setTimeout(() => {
        console.log("app compo profile updated");
        this.getuserdata();
      }, 2000);
    });
  }
  ngDoCheck() {
    if (
      !this.profile &&
      this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS)
    ) {
      this.getuserdata();
    }
  }

  ngOnInit() {
    this.getuserdata();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      var url = localStorage.getItem(storedConstants.DYNAMIC_BASE_URL);
      console.log(url);
      if (url) {
        this.rest.BaseUrl = url;
      }
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.ionViewDidLoad();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.getMenuData();
      this.menu.swipeEnable(false, "menu1");
      this.menu.swipeEnable(false, "menu2");

      this.connectivity.watchOnline().subscribe((net) => {
        if (net) {
          this.sharedService.presentToast("Your device is online");
        }
      });
      this.connectivity.watchOffline().subscribe((net) => {
        if (net) {
          this.sharedService.presentToast(
            "Please check your internet connection "
          );
        }
      });
    });
  }
  ionViewDidLoad() {
    // this.pushwooshNotification();
    if (!this.dataService.getFromLocalStorage(storedConstants.INTRO)) {
      this.nav.setRoot(IntroPage);

      this.dataService.localeStorage(storedConstants.INTRO, true);
    } else if (this.sharedService.checkForLoginStatus()) {
      this.nav.setRoot(HomePage);
      this.getuserdata();
      if (this.appVersion) {
        this.appVersion
          .getVersionNumber()
          .then((ver) => {
            console.log("application version ", ver);
            this.applicationVersion = ver.toString();
          })
          .catch((er) => {
            console.log(er);
            this.applicationVersion = "0.0.0";
          });
      }
    }
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  getuserdata() {
    if (this.sharedService.checkForLoginStatus()) {
      var user = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      );
      if (user) {
        console.log(user);
        this.user = user;
        this.profile = user.profile_info_for_profile
          ? user.profile_info_for_profile
          : null;
      }
    }
  }
  closeMenu() {
    this.menu.close();
  }

  getMenuData() {
    this.information = [
      {
        name: "Debut ",
        image: "./assets/imgs/menu-icon/home-unselect-icon.png",
        activeImage: "./assets/imgs/menu-icon/home-select-icon.png",
        children: [
          {
            name: "Acceuil",
          },
          {
            name: "Favoris ",
          },
        ],
      },
      {
        name: "MESSAGERIE ",
        image: "./assets/imgs/menu-icon/message-unselect-icon.png",
        activeImage: "./assets/imgs/menu-icon/message-select-icon.png",
        children: [
          {
            name: "Boite de réception ",
          },
          {
            name: "Nouveau ",
          },
          {
            name: "Envoyés ",
          },
          {
            name: "Communiqués  ",
          },
        ],
      },
      {
        name: "ECOLE ",

        image: "./assets/imgs/menu-icon/school-unselect-icon.png",
        activeImage: "./assets/imgs/menu-icon/school-select-icon.png",
        children: [
          {
            name: "Mes Enfants",
          },
          {
            name: "Calendrier/ Evénement  ",
          },
          {
            name: "A propos de l’Ecole ",
          },
        ],
      },

      {
        name: "RESEAU ",

        image: "./assets/imgs/menu-icon/network-unselect-icon.png",
        activeImage: "./assets/imgs/menu-icon/network-select-icon.png",
        children: [
          {
            name: "Parrainer un Tuteur",
          },
          {
            name: "Témoignages",
          },
        ],
      },
      {
        name: "PARTENAIRES SCOLAIRES",

        image: "./assets/imgs/menu-icon/school-partner-unselect.png",
        activeImage: "./assets/imgs/menu-icon/school-partner-select.png",
      },
      {
        name: "NAKELASI",

        image: "./assets/imgs/menu-icon/nakelasi-.png",
        activeImage: "./assets/imgs/menu-icon/nakelasi-.png",
      },
      // {
      //   name: "PROFILE",

      //   image: "./assets/imgs/profile-icon.png",
      //   activeImage: "./assets/imgs/profile-icon.png"
      // },
      {
        name: "REGLAGES",

        image: "./assets/imgs/menu-icon/setting-unselect-icon.png",
        activeImage: "./assets/imgs/menu-icon/setting-select-icon.png",
        children: [
          {
            name: "Profile ",
          },
          {
            name: "Statistiques  ",
          },
        ],
      },
      {
        name: "AIDE ",

        image: "./assets/imgs/menu-icon/help-unselect-icon.png",
        activeImage: "./assets/imgs/menu-icon/help-select-icon.png",
      },
    ];

    this.information2 = [
      {
        name: "Accueil ",
        image: "./assets/imgs/menu-icon/home-unselect-icon.png",
        activeImage: "./assets/imgs/menu-icon/home-select-icon.png",
      },
      {
        name: "MESSAGERIE ",
        image: "./assets/imgs/menu-icon/message-unselect-icon.png",
        activeImage: "./assets/imgs/menu-icon/message-select-icon.png",
      },
      {
        name: "Notification",

        image: "./assets/imgs/menu-icon/notification-unselect.png",
        activeImage: "./assets/imgs/menu-icon/notification-select.png",
        children: [
          {
            name: "Aujourd'hui",
          },
        ],
      },
      {
        name: "Profile",

        image: "./assets/imgs/menu-icon/profile-unselect-icon.png",
        activeImage: "./assets/imgs/menu-icon/profile-select-icon.png",
      },

      {
        name: "Favoris",

        image: "./assets/imgs/menu-icon/star-unselect.png",
        activeImage: "./assets/imgs/menu-icon/star-select.png",
      },

      // {
      //   name: "textes juridique",

      //   image: "./assets/imgs/menu-icon/help-unselect-icon.png",
      //   activeImage: "./assets/imgs/menu-icon/help-select-icon.png",
      // },
      //    {
      //   name: "en bref",
      //   image: "./assets/imgs/menu-icon/home-unselect-icon.png",
      //   activeImage: "./assets/imgs/menu-icon/home-select-icon.png",
      // },
      {
        name: "Mes jetons",
        image: "./assets/imgs/menu-icon/help-unselect-icon.png",
        activeImage: "./assets/imgs/menu-icon/help-select-icon.png",
      },
    ];
  }

  toggleSection(i) {
    this.information[i].open = !this.information[i].open;
    const index = i;
    if (this.information[i].open) {
      for (let i in this.information) {
        if (index != i) {
          this.information[i].open = false;
        }
      }
    }

    if (i == 4) {
      this.menu.close();

      this.nav.push(Partenaires);
    }
    if (i == 7) {
      this.menu.close();

      this.nav.push(Supports);
    }
    if (i == 5) {
      this.menu.close();

      this.nav.push(AproposPage);
    }
  }

  toggleSection2(i, j?) {
    console.log(i);

    this.information2[i].open = !this.information2[i].open;
    const index = i;
    if (this.information2[i].open) {
      for (let i in this.information2) {
        if (index != i) {
          this.information2[i].open = false;
        }
      }
    }

    if (i == 0) {
      this.menu.close();
      console.log(this.nav.getActive());
      let view = this.nav.getActive();
      if (view.instance instanceof HomePage) {
      } else {
        this.nav.setRoot(HomePage);
      }
    }
    if (i == 1) {
      this.menu.close();

      this.nav.push(MessagingPage, "mailBox");
    }
    if (i == 3) {
      this.menu.close();

      this.nav.push(UserprofilePage);
    }
    if (i == 2 && j == 0) {
      this.menu.close();

      this.nav.push(CalendarPage);
    }
    if (i == 4) {
      this.menu.close();
      this.nav.push(FavorisPage);
    }
    // if (i == 5) {
    //   this.menu.close();
    //   this.nav.push(LegalTextsPage);
    // }
    if (i == 5) {
      this.menu.close();
      this.nav.push(HistoryTokenPage);
    }
  }
  navToProfile() {
    this.menu.close();
    this.nav.push(UserprofilePage);
  }
  toggleItem(i, j) {
    if (i == 0 && j == 0) {
      this.menu.close();
      console.log(this.nav.getActive());
      let view = this.nav.getActive();
      if (view.instance instanceof HomePage) {
      } else {
        this.nav.setRoot(HomePage);
      }
    }

    if (i == 0 && j == 1) {
      this.menu.close();
      this.nav.push(FavorisPage);
    }
    console.log(i, j);
    if (i == 2 && j == 0) {
      this.menu.close();
      this.nav.push(ListPage);
    }
    if (i == 2 && j == 2) {
      this.menu.close();
      // this.nav.push(AboutSchoolPage);
    }
    if (i == 1 && j == 0) {
      this.menu.close();
      this.nav.setRoot(MessagingPage, "mailBox");
    }
    if (i == 1 && j == 1) {
      this.menu.close();
      this.nav.setRoot(MessagingPage);
      this.nav.push(CreateMessagePage);
    }
    if (i == 1 && j == 2) {
      this.menu.close();
      this.nav.setRoot(MessagingPage, "sentMessage");
    }
    if (i == 1 && j == 3) {
      this.menu.close();
      this.nav.push(SchoolNewsReleasesPage);
    }
    if (i == 2 && j == 2) {
      this.menu.close();

      this.nav.push(AboutEcole);
    }
    if (i == 3 && j == 0) {
      this.menu.close();

      this.nav.push(ParrainagesPage);
    }
    if (i == 3 && j == 1) {
      this.menu.close();

      this.nav.push(Temoignages);
    }
    if (i == 6 && j == 0) {
      this.navToProfile();
    }
    if (i == 6 && j == 1) {
      this.menu.close();

      this.nav.push(StatistiquesPage);
    }
    if (i == 2 && j == 1) {
      this.menu.close();

      this.nav.push(CalendarPage);
    }

    this.information[i].children[j].open = !this.information[i].children[j]
      .open;
  }
  logout() {
    this.profile = null;
    this.menu.close();
    this.nav.setRoot(LoginPage);
    localStorage.removeItem(storedConstants.USER_DETAILS);
  }

  //test push notification
  // pushwooshNotification(){
  //   let pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");
  //   let e = this.events;
  //   // Set push notifications handler.
  //   document.addEventListener('push-notification', function (event: any) {
  //     // console.log('user data 1: ' + JSON.stringify(event));

  //     let title = event.notification.title;
  //     let message = event.notification.message;
  //     let userData = event.notification.userdata;

  //     if (typeof (userData) != "undefined") {
  //       console.log('user data 1.1: ' + JSON.stringify(userData));
  //     }

  //   });
  //   pushNotification.onDeviceReady({
  //     projectid: this.GOOGLE_PROJECT_NUMBER,
  //     pw_appid: this.PUSHWOOSH_APP_ID
  //     // serviceName: "MPNS_SERVICE_NAME"
  //   });

  //   // Register for pushes.
  //   pushNotification.registerDevice(
  //     function (status) {
  //       var pushToken = status['pushToken'];
  //       console.log('push token: ' + pushToken);
  //       e.publish("Notification:Token", pushToken);
  //       // alert('push token: ' + JSON.stringify(pushToken));
  //     },
  //     function (status) {
  //       console.warn(JSON.stringify(['failed to register ', status]));
  //     }
  //   );
  //   document.addEventListener('push-receive', function (event: any) {
  //     let title = event.notification.title;
  //     let message = event.notification.message;
  //     let userData = event.notification.userdata;
  //     console.log('user data 2: ' + JSON.stringify(event));

  //     if (userData) {
  //       console.log('user data 2.1 : ' + JSON.stringify(userData));
  //     }
  //     if (message) {
  //       e.publish("NotificationPage", event);
  //     }
  //   });
  // }
}
