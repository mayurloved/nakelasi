import { Component } from '@angular/core';
import { RestProvider } from '../../providers/rest/rest';
import { DataProvider } from '../../providers/data/data';
import { storedConstants, constants } from '../../providers/constants';
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";

/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
    selector: 'page-legalTexts',
    templateUrl: 'legalTexts.html',
})
export class LegalTextsPage {

    userData:any =[];

    legalTextData:any =[];
    constructor(public restService: RestProvider, public dataService: DataProvider, private iab: InAppBrowser ) {
        this.userData = this.dataService.getFromLocalStorage(
            storedConstants.USER_DETAILS
        );
        console.log(this.userData, this.userData.data.id_titeur)
        this.getLegalTextData();
    }

    getLegalTextData(){
        let input = new FormData();
        input.append("id_titeur", this.userData.data.id_titeur);

        this.restService.postDataObservable(constants.LEGAL_TEXTS, input)
        .subscribe(res => {
            console.log(res);
            let res1:any = res.data;
            this.legalTextData = res1;
        }, err => {
            console.log(err)
        })
    }

    openLink(link){
        this.iab.create(link,'_blank','location=yes')
    }

}
