import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";
import { constants, storedConstants } from "../../providers/constants";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { DataProvider } from "../../providers/data/data";
import { NewsDetailPage } from "../news-detail/news-detail";

/**
 * Generated class for the EnPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: "page-en",
  templateUrl: "en.html",
})
export class EnPage {
  cards = [];
  id_titeur: any;
  randomArray:any;
  count = 1;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad EnPage");
  }

  ionViewWillEnter() {
    this.getTitle();
    // }
  }

  getTitle() {
    this.id_titeur = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    )
      ? this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS).data
          .id_titeur
      : "";
    let input = new FormData();
    if (this.id_titeur) {
      input.append("id_titeur", this.id_titeur);
    }
    input.append("page_count", this.count.toString());
    this.sharedService.presentLoading();
    this.restService.postDataObservable(constants.HOME, input).subscribe(
      (res) => {
        if (res.status) {
          console.log(res.status);
          this.cards = res.news_data.filter((id) => {
            id.created = id.created.replace(/\s/g, "T");
            return id;
          });
          this.sharedService.dismissLoader();
        } else {
          this.sharedService.presentToast(res.message);
          this.sharedService.dismissLoader();
        }
      },
      (err) => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        console.log(err);
      }
    );
  }

  doInfinite(infiniteScroll) {
    this.count++;
    console.log("Begin async operation");
    this.id_titeur = this.dataService.getFromLocalStorage(
      storedConstants.USER_DETAILS
    )
      ? this.dataService.getFromLocalStorage(storedConstants.USER_DETAILS).data
          .id_titeur
      : "";
    let input = new FormData();
    if (this.id_titeur) {
      input.append("id_titeur", this.id_titeur);
    }
    input.append("page_count", this.count.toString());
    // input.append("page_count_type", this.cat_count.toString());
    // input.append("news_type", this.tabcategory);
    this.restService.postDataObservable(constants.HOME, input).subscribe(
      (res) => {
        if (res.status) {
          // this.childrenData = res.child_data;
          let cards = res.news_data.filter((id) => {
            id.created = id.created.replace(/\s/g, "T");
            return id;
          });
          if (cards.length > 0) {
            for (let i = 0; i < cards.length; i++) {
              this.cards.push(cards[i]);
            }
          }
          this.sharedService.dismissLoader();
          infiniteScroll.complete();
        } else {
          this.sharedService.presentToast(res.message);
          this.sharedService.dismissLoader();
          infiniteScroll.complete();
        }
      },
      (err) => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        infiniteScroll.complete();
        console.log(err);
      }
    );
  }

  gotoNewsDetail(main) {
    this.randomGenerator(3, this.cards);
    if (this.randomArray) {
      this.navCtrl.push(NewsDetailPage, {
        main: main,
        // other: [this.cards[i + 1], this.cards[i + 2], this.cards[i + 3]]
        other: this.randomArray,
        cards: this.cards,
      });
    }
  }
  randomGenerator(i, array) {
    let card = [];
    for (let j = 0; j < i; j++) {
      card.push(array[Math.floor(Math.random() * array.length)]);
    }
    console.log(card);
    return this.checkFORUnique(card, i);
  }
  checkFORUnique(array, len) {
    var result = array
      .map((item) => item.id_news)
      .filter((value, index, self) => self.indexOf(value) === index);
    console.log(array);
    if (result.length !== len) {
      this.randomGenerator(len, this.cards);
      // return false;
    } else {
      this.randomArray = array;
    }
  }
}
