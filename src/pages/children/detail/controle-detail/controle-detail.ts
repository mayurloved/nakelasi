import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { SharedServiceProvider } from "../../../../providers/shared-service/shared-service";
import { RestProvider } from "../../../../providers/rest/rest";
import { constants } from "../../../../providers/constants";

/**
 * Generated class for the ControleDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-controle-detail",
  templateUrl: "controle-detail.html"
})
export class ControleDetailPage {
  controls_detail = [];
  childDetail: any;
  terms = "";
  finalControls_detail = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public rest: RestProvider,
    public sharedService: SharedServiceProvider
  ) {}

  ionViewWillEnter() {
    this.childDetail = this.navParams.data;
    if (this.childDetail.authen == "1") {
      this.getchildDetails(this.childDetail.id_eleve);
    }
  }
  ionChange(terms) {
    if (!terms) {
      this.controls_detail = this.finalControls_detail;
    }
    this.controls_detail = this.finalControls_detail;

    terms = terms.toLowerCase();
    this.controls_detail = this.controls_detail.filter(it => {
      return it.nom_controle.toLowerCase().includes(terms);
    });
  }
  getchildDetails(id) {
    this.sharedService.presentLoading();
    let input = new FormData();
    input.append("id_eleve", id);
    this.rest.postDataObservable(constants.CHILD_DETAIL, input).subscribe(
      res => {
        console.log(res);
        if (res.status) {
          this.controls_detail = res.controls_detail;
          this.finalControls_detail = res.controls_detail;

          this.sharedService.dismissLoader();
        } else {
          this.sharedService.presentToast(res.message);
          this.sharedService.dismissLoader();
        }
      },
      err => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        console.log(err);
      }
    );
  }
  doRefresh(refresher) {
    console.log(refresher);
    console.log("Begin async operation", refresher);

    this.ionViewWillEnter();
    setTimeout(() => {
      console.log("Async operation has ended");
      refresher.complete();
    }, 2000);
  }
}
