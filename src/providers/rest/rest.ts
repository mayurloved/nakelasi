import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { SharedServiceProvider } from "../shared-service/shared-service";
import { Observable } from "rxjs/Observable";
import { constants, storedConstants } from "../constants";
import { DataProvider } from "../data/data";

/*
 created by :Paras Shah
 created date: 19 Dec, 2018
 purpose : Http calls get,post etc.. with observable and promises. 
*/

@Injectable()
export class RestProvider {
  BaseUrl = constants.BASE_URL;

  constructor(
    public http: HttpClient,
    private SharedService: SharedServiceProvider,
    private dataService: DataProvider
  ) {
    // console.log("Hello RestProvider Provider");
  }
  //Headers
  getHeader() {
    if (this.SharedService.checkForLoginStatus()) {
      let key: any = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      );
      console.log(key.api_key);
      let headers = new HttpHeaders();
      console.log(headers);
      return headers;
    }
  }
  ///observables
  getDataObservable(url, options = {}): Observable<any> {
    this.SharedService.presentLoading();
    // options = this.getHeader();
    // console.log(this.getHeader());
    url = this.BaseUrl + url;
    return this.http.get(url, options);
  }
  postDataObservable(url, body = {}, options?): Observable<any> {
    options = this.getHeader();
    url = this.BaseUrl + url;
    return this.http.post(url, body, options);
  }
  putDataObservable(url, body = {}, options = {}): Observable<any> {
    url = this.BaseUrl + url;
    return this.http.put(url, body, options);
  }

  deleteDataObservable(url, options = {}): Observable<any> {
    url = this.BaseUrl + url;
    return this.http.delete(url, options);
  }
  ///Promises
  getDataPromise(url, options = {}): Promise<{}> {
    this.SharedService.presentLoading();
    url = this.BaseUrl + url;
    return new Promise(resolve => {
      this.http.get(url, options).subscribe(
        data => {
          resolve(data);
          this.SharedService.dismissLoader();
        },
        err => {
          console.log(err);
          this.SharedService.dismissLoader();
        }
      );
    });
  }

  postDataPromise(url, body = {}, options = {}): Promise<{}> {
    url = this.BaseUrl + url;
    return new Promise((resolve, reject) => {
      this.http.post(url, body, options).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
          reject(err);
        }
      );
    });
  }

  putDataPromise(url, body = {}, options = {}): Promise<{}> {
    url = this.BaseUrl + url;
    return new Promise(resolve => {
      this.http.put(url, body, options).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
        }
      );
    });
  }
}
