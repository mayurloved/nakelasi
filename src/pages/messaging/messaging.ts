import { Component } from "@angular/core";
import { NavController, NavParams, Events } from "ionic-angular";
import { CreateMessagePage } from "./create-message/create-message";
import { MessageDetail } from "./message-detail/message-detail";
import { RestProvider } from "../../providers/rest/rest";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { DataProvider } from "../../providers/data/data";
import { storedConstants, constants } from "../../providers/constants";

/**
 * Generated class for the MessagingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-messaging",
  templateUrl: "messaging.html"
})
export class MessagingPage {
  messageType = "mailBox";
  category_current: any;
  items = [];
  id_titeur: any;
  result: any;
  count = 1;
  nbn = 0;
  showSearch: boolean;
  terms: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider,
    public events: Events
  ) {}

  ionViewDidLoad() {
    this.events.subscribe("nbn", data => {
      this.nbn = data > 0 ? data : 0;
    });
  }
  ionViewWillEnter() {
    this.resetSearch();

    this.getMessages();
  }
  getMessages() {
    if (this.sharedService.checkForLoginStatus()) {
      this.sharedService.presentLoading();

      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      ).data.id_titeur;
      let input = new FormData();
      input.append("id_titeur", this.id_titeur);
      input.append("page_count", "1");
      this.restService.postDataObservable(constants.INBOX, input).subscribe(
        res => {
          if (res.status) {
            this.count++;
            console.log(res);
            this.result = res;
            this.items = res.inbox_mails.filter(id => {
              id.created_m = id.created_m.replace(/\s/g, "T");
              return id;
            });
            this.events.publish("reception_count", res.reception_count);
            this.events.publish("envoyes_count", res.envoyes_count);
            this.events.publish("corbeille_count", res.corbeille_count);
            this.events.publish("nbn", res.nbn);
            this.nbn = res.nbn;
            // this.cards = res.news_data;
            if (this.navParams.data) {
              this.messageType = this.navParams.data;
              this.segmentChanged(this.navParams.data);
            }
            this.sharedService.dismissLoader();
          } else {
            this.sharedService.dismissLoader();
          }
        },
        error => {
          this.sharedService.presentToast(error["error"].message);
          this.sharedService.dismissLoader();
        }
      );
    }
  }
  fnGotoCreateMessage() {
    this.navCtrl.push(CreateMessagePage);
  }
  setcolor(category) {
    if (category == "frais") {
      return "frais";
    } else if (category == "etudes") {
      return "etudes";
    } else {
      return "autres";
    }
  }
  goToMessageDetail(i) {
    this.navCtrl.push(MessageDetail, i);
    // this.na
  }
  segmentChanged(type) {
    console.log(this.messageType);
    if (this.messageType == "mailBox") {
      this.items = this.result.inbox_mails.filter(id => {
        id.created_m = id.created_m.replace(/\s/g, "T");
        return id;
      });
    } else if (this.messageType == "sentMessage") {
      this.items = this.result.send_mails.filter(id => {
        id.sended_m = id.sended_m.replace(/\s/g, "T");
        return id;
      });
    }
  }
  doRefresh(refresher) {
    this.resetSearch();
    this.count = 1;
    console.log(refresher);
    console.log("Begin async operation", refresher);

    this.getMessages();
    setTimeout(() => {
      console.log("Async operation has ended");
      refresher.complete();
    }, 2000);
  }

  doInfinite(infiniteScroll) {
    this.resetSearch();
    if (this.sharedService.checkForLoginStatus()) {
      // this.sharedService.presentLoading();

      this.id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      ).data.id_titeur;
      let input = new FormData();
      input.append("id_titeur", this.id_titeur);
      input.append("page_count", this.count.toString());
      this.restService.postDataObservable(constants.INBOX, input).subscribe(
        res => {
          if (res.status) {
            console.log(res);
            this.count++;
            // this.result = res;
            if (res.inbox_mails.length > 0) {
              let inbox = res.inbox_mails.filter(id => {
                id.created_m = id.created_m.replace(/\s/g, "T");
                return id;
              });
              for (let i = 0; i < inbox.length; i++) {
                this.result.inbox_mails.push(inbox[i]);
              }
            }

            if (res.send_mails.length > 0) {
              let sent = res.send_mails.filter(id => {
                id.sended_m = id.sended_m.replace(/\s/g, "T");
                return id;
              });
              for (let j = 0; j < sent.length; j++) {
                this.result.send_mails.push(sent[j]);
              }
            }
            // this.events.publish("reception_count", res.reception_count);
            // this.events.publish("envoyes_count", res.envoyes_count);
            // this.events.publish("corbeille_count", res.corbeille_count);
            // this.events.publish("nbn", res.nbn);
            // this.cards = res.news_data;
            this.segmentChanged(this.messageType);
            this.sharedService.dismissLoader();
          } else {
            this.sharedService.dismissLoader();
          }
          infiniteScroll.complete();
        },
        error => {
          this.sharedService.presentToast(error.message);
          this.sharedService.dismissLoader();
          infiniteScroll.complete();
        }
      );
    }

    console.log("Async operation has ended");
  }
  resetSearch() {
    this.showSearch = false;
    this.terms = "";
  }
}
