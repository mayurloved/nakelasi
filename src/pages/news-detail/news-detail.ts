import { Component, ViewChild } from "@angular/core";
import { NavController, NavParams, Content } from "ionic-angular";
import { DataProvider } from "../../providers/data/data";
import { SharedServiceProvider } from "../../providers/shared-service/shared-service";
import { RestProvider } from "../../providers/rest/rest";
import { constants, storedConstants } from "../../providers/constants";
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the NewsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 * https://medium.com/@AAlakkad/angular-2-display-html-without-sanitizing-filtering-17499024b079
 */

@Component({
  selector: "page-news-detail",
  templateUrl: "news-detail.html"
})
export class NewsDetailPage {
  @ViewChild(Content) content: Content;
  isShow = false;
  newsDetails: any;
  comment_flag;
  newsOthers: any;
  cards: any;
  randomArray: any;
  comment = [];
  loggedInEmail: any;
  comment_text: string | Blob = "";
  pseudo: string | Blob;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestProvider,
    public sharedService: SharedServiceProvider,
    public dataService: DataProvider,
    private socialSharing: SocialSharing
  ) {
    console.log(navParams);
    this.newsDetails = navParams ? navParams.data.main : null;
    this.newsOthers = navParams ? navParams.data.other : null;
    this.cards = navParams ? navParams.data.cards : null;
  }
  back() {
    this.navCtrl.pop();
  }
  getPrefix(word) {
    return word.substring(0, 2);
  }
  getLessCorps(word) {
    const description = word;
    return description.substring(0, description.length / 6);
  }
  gotoNewsDetail(main) {
    this.randomGenerator(3, this.cards);

    // this.navCtrl.pop();
    if (this.randomArray) {
      // this.navCtrl.push(NewsDetailPage, {
      //   main: main,
      //   // other: [this.cards[i + 1], this.cards[i + 2], this.cards[i + 3]]
      //   other: this.randomArray,
      //   cards: this.cards
      // });
      this.newsDetails = main;
      this.newsOthers = this.randomArray;
    }
    this.content.scrollToTop();
    this.getComments();
    this.isShow = false;
  }
  randomGenerator(i, array) {
    let card = [];
    for (let j = 0; j < i; j++) {
      card.push(array[Math.floor(Math.random() * array.length)]);
    }
    console.log(card);
    return this.checkFORUnique(card, i);
  }
  checkFORUnique(array, len) {
    var result = array
      .map(item => item.id_news)
      .filter((value, index, self) => self.indexOf(value) === index);
    console.log(array);
    if (result.length !== len) {
      this.randomGenerator(len, this.cards);
      // return false;
    } else {
      this.randomArray = array;
    }
  }
  ionViewWillLoad() {
    this.getComments();
    this.isShow = false;
    console.log("ionViewDidLoad NewsDetailPage");
  }
  linkify(text) {
    var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
    return text.replace(urlRegex, function (url) {
      return '<a href="' + url + '">' + url + "</a>";
    });
  }
  getComments() {
    this.comment = [];

    // if (this.sharedService.checkForLoginStatus()) {
    // this.sharedService.presentLoading();
    this.comment_flag = false;
    // this.news_id = this.dataService.getFromLocalStorage(
    //   storedConstants.USER_DETAILS
    // ).data.id_titeur;
    let input = new FormData();
    input.append("news_id", this.newsDetails.id_news);

    this.restService.postDataObservable(constants.ARTICLE, input).subscribe(
      res => {
        if (res.status) {
          this.comment = res.news_comment.filter(id => {
            id.created_com = id.created_com.replace(/\s/g, "T");
            return id;
          });
          this.newsDetails.vus = res.news_data.vus;
        } else {
          this.sharedService.presentToast(res.message);
        }
        this.comment_flag = true;
      },
      err => {
        this.sharedService.presentToast(err.error.message);
        this.sharedService.dismissLoader();
        this.comment_flag = true;

        console.log(err);
      }
    );
    // }
  }
  
  postComments() {
    if (this.comment_text && this.pseudo) {
      if (this.sharedService.checkForLoginStatus()) {
        this.sharedService.presentLoading();
        this.loggedInEmail = this.dataService.getFromLocalStorage(
          storedConstants.USER_DETAILS
        ).data.email;
        let input = new FormData();
        input.append("id_news", this.newsDetails.id_news);
        input.append("contenu", this.comment_text);
        input.append("pseudo", this.pseudo);
        input.append("email", this.loggedInEmail);

        if (this.loggedInEmail) {
          this.restService
            .postDataObservable(constants.ARTICLE, input)
            .subscribe(
              res => {
                if (res.status) {
                  this.comment = res.news_comment;
                  this.pseudo = null;
                  this.comment_text = null;
                  this.sharedService.dismissLoader();
                } else {
                  this.sharedService.presentToast(res.message);
                  this.sharedService.dismissLoader();
                }
              },
              err => {
                this.sharedService.presentToast(err.error.message);
                this.sharedService.dismissLoader();
                console.log(err);
              }
            );
        } else {
          this.sharedService.presentToast("Logged in email error");
        }
      }
    } else {
      this.sharedService.presentToast("Erreur: Le formulaire est vide ");
    }
  }
  favChange(newsId) {
    console.log(newsId)
    if (this.sharedService.checkForLoginStatus() && newsId) {


      this.sharedService.presentLoading();
      let id_titeur = this.dataService.getFromLocalStorage(
        storedConstants.USER_DETAILS
      ).data.id_titeur;
      let input = new FormData();
      input.append("titeur_id", id_titeur);
      input.append("news_id", newsId.id_news);
      this.restService.postDataObservable(constants.UPDATEFAVOURITE, input).subscribe(
        res => {
          if (res.status) {
            newsId.favorite = newsId.favorite == 1 ? 0 : 1;
            this.sharedService.presentToast(res.message);
            this.sharedService.dismissLoader();

          } else {
            this.sharedService.presentToast(res.message);

            this.sharedService.presentToast(res.message);
            this.sharedService.dismissLoader();

          }
        },
        err => {
          this.sharedService.presentToast(err.error.message);
          this.sharedService.dismissLoader();

          console.log(err);
        }
      );
    }
  }

  againGetData(news){
    let input = new FormData();
    input.append("news_id", news.id_news);
    this.restService.postDataObservable(constants.ARTICLE, input).subscribe(
      res => {
        let res1: any = res.news_data;
        this.newsDetails.share_count = res1.share_count;
      });
  }

  share(news, share_link) {
    event.stopPropagation();

    this.sharedService.presentLoading();

    this.socialSharing.share(share_link).then(res => {

      let input = new FormData();
      input.append("news_id", news.id_news);
      this.restService.postDataObservable(constants.SHARE_COUNT, input)
        .subscribe(res => {
          this.againGetData(news);
        }, err => {

        })
      this.sharedService.dismissLoader();
    }).catch((err) => {
      this.sharedService.dismissLoader();

    });

  }
}
